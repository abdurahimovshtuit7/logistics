import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import createStore from './src/services/createStore';
import * as Sentry from "@sentry/react-native";

import {
  StatusBar,
  View,
  Text
} from 'react-native';

import colors from './src/assets/styles/colors';
import LaunchScreen from 'screens/Auth/LaunchScreen/index_without_connect';
import OneSignal from "react-native-onesignal";


import AppNavigators from './src/navigators/AppNavigators';
// import LaunchScreen from './src/screens/LaunchScreen/index_without_connect';
import NavigationService from './src/navigators/NavigationService';
import {I18nextProvider} from 'react-i18next';
import i18n from 'services/i18n';
import {NavigationContainer} from '@react-navigation/native';

const {store, persistor} = createStore();
// const ID = await OneSignal.getUserId()


// throw new Error("My first Sentry error!");
// Sentry.nativeCrash();

class App extends Component {
  constructor(props) {
    super(props);
    // OneSignal.setLogLevel(6, 0);
    // OneSignal.inFocusDisplaying(2);

    this.onReceived = this.onReceived.bind(this);
    this.onOpened = this.onOpened.bind(this);
    this.onIds = this.onIds.bind(this);

    OneSignal.init('140a7409-c287-4ff0-98a4-3dd919a3fb36');
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(0);
    // OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);
    // OneSignal.configure();
    //https://github.com/facebook/react-native/pull/25146#issuecomment-533995604 ios issue
  }
  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }
  onReceived(notification) {
    console.log("Notification received: ", notification);
  }
  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }
  onIds(device) {
    console.log('Device info: ', device);
    // this.setState({
    //   pid: device.userId,
    // })
    // console.log(device.userId);
  }

  render() {
    // console.log("ID:",ID)
    return (
        <Provider store={store}>
          <NavigationContainer ref={NavigationService._navigator}>
            <PersistGate loading={<LaunchScreen/>} persistor={persistor}>
              <I18nextProvider i18n={i18n()}>
                <StatusBar
                    backgroundColor={'transparent'}
                    barStyle="light-content"
                    translucent={false}
                />
                <AppNavigators />
              </I18nextProvider>
            </PersistGate>
          </NavigationContainer>
        </Provider>
    );
  }
}

export default App;
