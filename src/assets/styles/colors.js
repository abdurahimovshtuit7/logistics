const brandColor = '#4E7DF1';
const white = '#fff';
const green = '#1B3783'
const brandGreen = '#27B872'
// const green = '#27B872';
const border= 'rgba(179, 198, 212, 0.5)';
const linearBegin = 'rgba(39, 80, 184, 0.61)';
const scale = 'rgba(0, 0, 0, 0.8)'
const linearEnd = 'rgba(0, 0, 0, 0)'
const footerColor = '#555555'
const text ='#B3C6D4'
const background ='rgba(255, 255, 255, 0.08)'
const red = '#FF4949'
const font = '#8D9BA8'
const whiteAlpha = 'rgba(255, 255, 255, 0.79)'
const grey = '#7E7E7E'
const lightBlack = '#394A64'
const blue ='#4F80FF'
const black = '#0E0E0E'
const extraGrey ="#646464"
const timeColor = '#212121'
const End = 'rgba(39, 80, 184, 0.4)'
const gradient = 'rgba(39, 80, 184, 0.3)'
const statusbar = 'rgba(39, 80, 184, 0.61)'
const darkBlue = '#1B3783'
const button = '#00FFEF'
// const end = ''


export default {
  brandColor,
  white,
  green,
  border,
  footerColor,
  linearBegin,
  linearEnd,
  text,
  background,
  red,
  font,
  whiteAlpha,
  grey,
  lightBlack,
  blue,
  black,
  extraGrey,
  timeColor,
  End,
  gradient,
  statusbar,
  scale,
  darkBlue,
  brandGreen,
  button,
  backgroundTransparent: 'transparent',
  mainColor: "#0550ea",
  carrot: '#e67e22',
  emerald: '#2ecc71',
  peterRiver: '#3498db',
  wisteria: '#8e44ad',
  alizarin: '#e74c3c',
  turquoise: '#1abc9c',
  midnightBlue: '#2c3e50'
};
