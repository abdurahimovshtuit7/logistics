import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = (props) => {
    return (
        <Svg width={17} height={18} viewBox="0 0 17 18" fill="none" {...props}>
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M8.333 17.333a8.333 8.333 0 110-16.666 8.333 8.333 0 110 16.666zm0-1.666a6.667 6.667 0 100-13.334 6.667 6.667 0 000 13.334zm-4.366-6.87l3.535 3.536 5.893-5.892-1.18-1.178-4.713 4.714-2.357-2.358-1.178 1.179z"
                fill={props.color}
            />
        </Svg>
    );
}

export default SvgComponent;
