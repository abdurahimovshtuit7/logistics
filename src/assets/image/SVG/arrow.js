import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = (props) => {
    return (
        <Svg width={10} height={16} viewBox="0 0 10 16" fill="none" {...props}>
            <Path d="M1 0.513458L8.5 8.01346L1 15.5135" stroke={props.color} />
        </Svg>
    );
}

export default SvgComponent;
