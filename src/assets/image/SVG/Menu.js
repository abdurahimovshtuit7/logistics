import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={35} height={11} viewBox="0 0 35 11" fill="none" {...props}>
            <Path fill="#fff" d="M0 0H35V2H0z" />
            <Path fill="#fff" d="M0 9H35V11H0z" />
        </Svg>
    );
}

export default SvgComponent;
