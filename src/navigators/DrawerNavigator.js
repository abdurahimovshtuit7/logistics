import React, {Component} from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {
    TouchableOpacity,
    Text
} from 'react-native'


import {createDrawerNavigator} from '@react-navigation/drawer';
import {connect} from "react-redux";

import NavigationService from "./NavigationService";
import {withTranslation} from "react-i18next";

import colors from 'assets/styles/colors'
//------Screens------------
import MainPage from 'screens/Main/MainPage'
import Select from 'screens/Main/Select'
// import Settings from 'screens/Home/Settings'
import DrawerContent from './DrawerContent';
import Profile from 'screens/Main/Profile'
import SelectTime from 'screens/Main/SelectTime'
import EditPassword from 'screens/Main/EditPassword'


const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();


const Main = (props) => (
    <Stack.Navigator>
        <Stack.Screen name='mainPage' component={MainPage}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='profile' component={Profile}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='editPassword' component={EditPassword}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='select' component={Select}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='selectTime' component={SelectTime}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />

    </Stack.Navigator>
);


let DrawerNav = (props) => (
    <Drawer.Navigator
        drawerStyle={{
            backgroundColor: colors.white,
        }}
        drawerContent={props => <DrawerContent {...props} />}
        // hideStatusBar={true}
    >
        <Drawer.Screen name="main" component={Main}
                       options={{
                           headerShown: false,
                       }}
        />
        {/*<Drawer.Screen name="profil"*/}
        {/*               component={Settings}*/}
        {/*               options={({route}) => ({*/}
        {/*                   //title: route.params.title,*/}
        {/*                   headerStyle: {*/}
        {/*                       backgroundColor: '#6F90B7',*/}
        {/*                   },*/}
        {/*                   headerTintColor: '#fff',*/}
        {/*               })}*/}
        {/*/>*/}
    </Drawer.Navigator>
)


const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        // user:state.user.user,

    };
};

DrawerNav = connect(mapStateToProps)(DrawerNav)
export default withTranslation('main')(DrawerNav);
