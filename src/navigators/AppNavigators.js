import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import LaunchScreen from 'screens/Auth/LaunchScreen/index';

import NavigationService from './NavigationService';
import {get} from 'lodash'
//------------SCREENS--------------
import IntroductionPage from 'screens/Auth/IntroductionPage';
import Login from 'screens/Auth/Login'
import SmsVerify from 'screens/Auth/smsVerify'
import SignUp from 'screens/Auth/SignUp'
import PasswordScreen from "screens/Auth/PasswordScreen";
import SearchScreen from 'screens/Admin/SearchScreen'
import Filter from 'screens/Admin/Filter'
import SingleItem from 'screens/Admin/SingleItem'
import SignIn from "screens/Auth/SignIn";
import EditPassword from 'screens/Main/EditPassword'


import {connect} from 'react-redux';
import DrawerNav from "./DrawerNavigator";

import i18next from 'i18next';
import {CommonActions} from '@react-navigation/native';
import {Platform, StatusBar} from 'react-native';
import colors from '../assets/styles/colors';


const Stack = createStackNavigator();

const Auth = (props) => (
    <Stack.Navigator>
        <Stack.Screen name='introduction' component={IntroductionPage}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='login' component={Login}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='smsVerify' component={SmsVerify}
                      options={{
                          headerShown: false,
                      }}
        />
        {/*<Stack.Screen name='codeEntry' component={CodeEntry}*/}
        {/*              options={{*/}
        {/*                  headerShown: false,*/}
        {/*              }}*/}
        {/*/>*/}
        <Stack.Screen name='signUp' component={SignUp}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='password' component={PasswordScreen}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='admin' component={SearchScreen}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='filter' component={Filter}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='singleItem' component={SingleItem}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='signIn' component={SignIn}
                      options={{
                          headerShown: false,
                      }}
        />
    </Stack.Navigator>
);
const Admin = () => (
    <Stack.Navigator>

        <Stack.Screen name='admin' component={SearchScreen}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='filter' component={Filter}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='singleItem' component={SingleItem}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
    </Stack.Navigator>
)

class AppNavigators extends Component {

    render() {
        const {userData} = this.props
        console.log("user:", userData)
        return (
            <Stack.Navigator>
                <Stack.Screen name='launchScreen' component={LaunchScreen}
                              options={{
                                  gestureEnabled:true,
                                  headerShown: false,
                              }}
                />

                {
                    get(userData, 'token', '') ? (
                        (get(userData, 'role', '') === 'driver') ? (<Stack.Screen name='drawer' component={DrawerNav}
                                                                                  options={({route}) => ({
                                                                                      headerShown: false,
                                                                                  })}
                        />) : <Stack.Screen name={'admin'} component={Admin}
                                            options={({route}) => ({
                                                headerShown: false,
                                            })}
                        />

                    ) : <Stack.Screen name='auth' component={Auth}
                                      options={({route}) => ({
                                          headerShown: false,
                                      })}
                    />
                }
            </Stack.Navigator>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        userData: state.user.user,
    };
};

export default connect(mapStateToProps)(AppNavigators);
