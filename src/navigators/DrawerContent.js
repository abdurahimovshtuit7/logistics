import React, {Component, useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    AsyncStorage,
    Linking, ImageBackground,
} from 'react-native';
import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
} from 'react-native-paper';

import {
    DrawerContentScrollView,
    DrawerItem,
    DrawerItemList,

} from '@react-navigation/drawer';
import Components from 'components';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import styles from './styles';
import NavigationService from './NavigationService';
import LinearGradient from 'react-native-linear-gradient';
import {get} from 'lodash';
import {saveUser} from 'services/actions';
import colors from '../assets/styles/colors';
import {SET_LANGUAGE} from "services/constants";
import {Routines} from "../services/api";
import OneSignal from "react-native-onesignal";



class DrawerContent extends Component {
    constructor(props) {
        super(props);
        this.onIds = this.onIds.bind(this);
        OneSignal.addEventListener('ids', this.onIds);
    }
    componentWillUnmount() {
        OneSignal.removeEventListener('ids', this.onIds);
    }
    state = {
        id:'',
        item1:[
            {label: 'RU', value: 'ru'},
            {label: 'UZ', value: 'uz'},

        ],
    }
    onIds(device) {
        console.log('Device info: ', device);
        console.log(device.userId);
        this.setState({id:device.userId})
    }
    onLangClicked = (lang) => {
        // console.log("LANG:",lang)
        let {i18n,dispatch} = this.props;
        i18n.changeLanguage(lang,()=>{
            dispatch({type:SET_LANGUAGE,lang})
        })
    }

    LogOut (){
        const oneID = this.state.id
        Routines.auth.logOUT({
            request: {
                data: {
                    oneID:oneID
                }
            }
        }, this.props.dispatch)
            .then((data) => {
                this.props.dispatch(saveUser({}));
            })
            .catch(e => {
                console.log('Error:',e)
                let status = get(e,'message.error','Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                // setSubmitting(false);
            });
    }

    render() {
        let {userData,t,currentLangCode,navigation} = this.props;
        console.log("Lang:",currentLangCode)
        let {item1} = this.state
        return (
            <View style={{flex: 1,}}>
                {/*<LinearGradient colors={[colors.linearBegin,colors.End,colors.gradient,colors.linearEnd]}*/}
                {/*                style={styles.wrapper}*/}
                {/*>*/}
                <DrawerContentScrollView {...this.props}>
                        <Components.SelectLanguage
                            zIndex={3}
                            items={item1}
                            placeholder={currentLangCode==='uz'?'UZ':'RU'}
                            select={(value)=>this.onLangClicked(value)}
                            currentLangCode={currentLangCode}

                        />

                    <View style={styles.content}>
                        <TouchableOpacity style={styles.item} onPress={()=>{
                            navigation.closeDrawer()
                        }}>
                            <Text style={styles.title}>
                                {t('asosiy')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.item} onPress={()=>{
                            NavigationService.navigate('profile')
                        }}>
                            <Text style={styles.title}>
                                {t('profil')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.item} onPress={()=>{

                        }}>
                            <Text style={styles.title}>
                                {t('about')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.item} onPress={()=>{
                           this.LogOut()
                        }}>
                            <Text style={styles.title}>
                                {t('chiqish')}
                            </Text>
                        </TouchableOpacity>
                    </View>



                    {/*<View>*/}
                    {/*    <Text style={styles.text}>{t('Til_tanlash')}</Text>*/}
                    {/*    <View style={styles.rows}>*/}
                    {/*        <TouchableOpacity style={[styles.touchable,{backgroundColor:(currentLangCode==='uz')?colors.brandColor:'#c7c7c7'}]} onPress={() => {*/}
                    {/*            this.onLangClicked('uz')*/}
                    {/*        }}>*/}
                    {/*            <Text style={styles.langText}>*/}
                    {/*                UZB*/}
                    {/*            </Text>*/}
                    {/*        </TouchableOpacity>*/}
                    {/*        <TouchableOpacity style={[styles.touchable,{backgroundColor:(currentLangCode==='ru')?colors.brandColor:'#c7c7c7'}]} onPress={() => {*/}
                    {/*            this.onLangClicked('ru')*/}
                    {/*        }}>*/}
                    {/*            <Text style={styles.langText}>*/}
                    {/*                РУС*/}
                    {/*            </Text>*/}
                    {/*        </TouchableOpacity>*/}
                    {/*    </View>*/}
                    {/*</View>*/}

                </DrawerContentScrollView>
                {/*</LinearGradient>*/}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        currentLangCode:state.language.lang,
        userData:state.user.user
    };
};

DrawerContent = connect(mapStateToProps)(DrawerContent);
export default withTranslation('main')(DrawerContent);
