const NAME = 'Logistics';
export const LOGOUT = `${NAME}/LOGOUT`;
export const SET_LANGUAGE = `${NAME}/SET_LANGUAGE`;
export const SAVE_USER = `${NAME}/SAVE_USER`
export const IMAGE_UPLOAD = `${NAME}/IMAGE_UPLOAD`
export const IMAGE_UP = `${NAME}/IMAGE_UP`
export const USER_ID = `${NAME}/USER_ID`

