const title = (currentLangCode, title_ru, title_uz, defaultTitle) => {

    switch (true) {

        case (currentLangCode === 'ru' && !!title_ru):
            return title_ru;
        case (currentLangCode === 'uz' && !!title_uz):
            return title_uz;
        default: {
            return defaultTitle
        }
    }
};

export default {
    title
}
