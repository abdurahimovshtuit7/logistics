import {
   Login,
    signUp,
    updateDetails,
    updateStatus,
    editPassword,
    aboutMe,
    getItems,
    search,
    getPickerData,
    filter
} from '../api/Auth/routines'
import { SET_LANGUAGE, LOGOUT } from '../constants'
import TokenStorage from '../TokenStorage'
import get from 'lodash/get'

const initial = {
    Login: {},
    signUp: {},
    edit: {},
    info:{},
    aboutOfMe:{},
    items: {
        data:[],
        pagination: {}
    },
    filter: {
        data:[],
        pagination: {}
    },
    search:{},
    getPickerData:{}
};

let pretty_message = (_message = {}, token) => {
    _message._id = _message.id;
    _message.text = _message.message;
    _message.createdAt = _message.created_at;
    _message.user = {};
    if(_message.type === 1){
        _message.user._id = 0;
    }
    else {
        _message.user._id = 1;
    }
    return _message
};

export default (state = initial, action) => {
    switch (action.type){
        case Login.SUCCESS:{
            let data = get(action, 'payload.response.data', {});
            return {
                ...state,
                Login: data
            }
        }
        case signUp.SUCCESS:{
            let data = get(action, 'payload.response.data', {});

            // console.log("getAllData", data)

            return {
                ...state,
                signUp: data
            }
        }
        case updateDetails.SUCCESS:{
            let data = get(action, 'payload.response.data', {});

            console.log("getAllData", data)

            return {
                ...state,
                info: data
            }
        }
        case updateStatus.SUCCESS:{
            let Token = get(action, 'payload.response.data', {});

            // console.log("getTOKEN", Token)

            return {
                ...state,
                token: Token
            }
        }
        case editPassword.SUCCESS:{
            let data = get(action, 'payload.response.data', {});


            return {
                ...state,
              edit:data
            }
        }
        case aboutMe.SUCCESS:{
            let data = get(action, 'payload.response.data', {});
            return {
                ...state,
                aboutOfMe:data
            }
        }
        case getPickerData.SUCCESS:{
            let data = get(action, 'payload.response.data', {});
            return {
                ...state,
                getPickerData:data
            }
        }
        case search.SUCCESS:{
            let data = get(action, 'payload.response.data', {});
            return {
                ...state,
                search:data
            }
        }
        case getItems.SUCCESS:{
            let olderData = [];
            let data = get(action, 'payload.response.data.data', []);
            let pagination = get(action,'payload.response.data',{});
            let {page} = get(action,'payload.request')
            console.log('PAGES:',page)
            console.log('Pagination:',pagination)

            if(page !== 1)
                olderData = get(state,'items.data',[])
            return {
                ...state,
                items:{
                    data:[...olderData,...data],
                    pagination
                }
            }

        }
        case filter.SUCCESS:{
            let olderData = [];
            let data = get(action, 'payload.response.data.data', []);
            let pagination = get(action,'payload.response.data',{});

            let {page} = get(action,'payload.request.queryParams')
            console.log('PAGES:',page)
            console.log('Pagination:',pagination)

            if(page!== 1)
                olderData = get(state,'filter.data',[])
            return {
                ...state,
                filter:{
                    data:[...olderData,...data],
                    pagination
                }
            }

        }
        case SET_LANGUAGE:{
            return {
                ...state,
                currentLangCode: action.lng
            }
        }
        case LOGOUT: {
            TokenStorage.clear();
            return initial
        }
    }
    return state
}
