import {SET_LANGUAGE,SAVE_USER,IMAGE_UPLOAD,IMAGE_UP,USER_ID} from "../constants"

export const changeLanguage = lang => ({
    type:SET_LANGUAGE,
    lang,
});


export const saveUser = user => ({
    type:SAVE_USER,
    user,
})

export const image = image => ({
    type:IMAGE_UPLOAD,
    image,
})
export const tex_pass = tex_pass => ({
    type:IMAGE_UP,
    tex_pass,
})
export const user_id = user_id => ({
    type:USER_ID,
    user_id,
})
