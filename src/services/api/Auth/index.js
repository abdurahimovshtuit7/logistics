import buildUrl from 'build-url'

export default (api) => {
    return {
        Login: (data) => {
            return api.post(`/api/v1/auth/login`, data.data)
        },
        signUp: (data) => {
            return api.post(`/api/v1/auth/register`,data.data)
        },
        aboutMe: () => {
            return api.get(`/api/v1/auth/me`)
        },
        updateDetails: (data) => {
            return api.put(`/api/v1/auth/updatedetails`,data.data)
        },
        updateStatus:(data) => {
            return api.put(`/api/v1/auth/updatestatus`,data.data)
        },
        confirm: (data)=>{
            return api.post(`/auth/confirm`, data.data)
        },
        editPassword:(data)=>{
            return api.post(`/api/auth/editpassword`,data.data)
        },
        logOUT:(data)=>{
            return api.post(`/api/v1/auth/logout`,data.data)
        },
        getItems:(data)=>{
            let url = buildUrl('/api/v1/users',{
                queryParams:{
                    page:data.page,
                }
            })
            return api.get(url)
        },
        updateProfile:(data) => {
          return api.put(`/api/v1/auth/editprofile`,data.data)
        },
        search:(data)=>{
            let url = buildUrl('/api/v1/users/search',{
                queryParams:{
                    search:data.search,
                }
            })
            return api.post(url)
        },
        Delete:(data)=>{
            return api.delete(`/api/v1/users/${data.id}`,data.data)

        },
        getPickerData: () => {
            return api.get(`/api/v1/language`)
        },
        filter:(data)=>{
                let {queryParams} = data
                let url = buildUrl('/api/v1/users',{
                    queryParams

                })
                return api.get(url)
        },
    }
}
