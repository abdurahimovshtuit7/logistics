import { call, put, takeEvery, all } from 'redux-saga/effects'

import { updateProfile } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(updateProfile.request());
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.auth.updateProfile, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                updateProfile.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(updateProfile.failure(e))
    } finally {
        yield put(updateProfile.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(updateProfile.TRIGGER, trigger, api)
}
