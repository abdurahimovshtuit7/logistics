import { call, put, takeEvery, all } from 'redux-saga/effects'

import { updateDetails } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(updateDetails.request());
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.auth.updateDetails, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                updateDetails.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(updateDetails.failure(e))
    } finally {
        yield put(updateDetails.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(updateDetails.TRIGGER, trigger, api)
}
