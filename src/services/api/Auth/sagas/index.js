import { all } from "redux-saga/effects";

import Login from './Login'
import updateStatus from './updateStatus'
import updateDetails from './updateDetails'
import signUp from './signUp'
import editPassword from "./editPassword";
import aboutMe from  "./aboutMe"
import getItems from "./getItems";
import search from './search'
import Delete from './Delete'
import getPickerData from  './getPickerData'
import filter from './filter'
import logOUT from "./logOUT";
import updateProfile from  './updateProfile'


export default function * sagas (api) {
    yield all(
        [
            Login(api),
            signUp(api),
            updateDetails(api),
            updateStatus(api),
            editPassword(api),
            aboutMe(api),
            getItems(api),
            search(api),
            Delete(api),
            getPickerData(api),
            filter(api),
            logOUT(api),
            updateProfile(api)
        ]
    )
}
