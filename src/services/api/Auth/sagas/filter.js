import { call, all, put, takeEvery } from 'redux-saga/effects'

import { filter } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(filter.request());

        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.auth.filter, request);
        // console.log("wwwww",JSON.stringify(response.data));
        yield all([
            put(
                filter.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(filter.failure(e))
    } finally {
        yield put(filter.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(filter.TRIGGER, trigger, api)
}
