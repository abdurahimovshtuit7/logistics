import { call, put, takeEvery, all } from 'redux-saga/effects'

import { search } from '../routines'
import TokenStorage from '../../../TokenStorage';
import {get} from "lodash";

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(search.request());
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)
        const response = yield call(api.auth.search, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                search.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(search.failure(e))
    } finally {
        yield put(search.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(search.TRIGGER, trigger, api)
}
