import { call, all, put, takeEvery } from 'redux-saga/effects'

import { getPickerData } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(getPickerData.request());

        // const currentToken = yield call(TokenStorage.get)
        //
        // if(currentToken)
        //     yield call(api.setToken,currentToken)

        const response = yield call(api.auth.getPickerData, request);
        // console.log("wwwww",JSON.stringify(response.data));
        yield all([
            put(
                getPickerData.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(getPickerData.failure(e))
    } finally {
        yield put(getPickerData.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(getPickerData.TRIGGER, trigger, api)
}
