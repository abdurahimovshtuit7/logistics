import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {statusBarHeigth} from "../../../assets/styles/commons";

const styles = StyleSheet.create({
    container:{
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        paddingTop:statusBarHeigth,

        // paddingHorizontal:20,
        backgroundColor:colors.white

    },
    header:{
        flexDirection:'row',
        alignItems:'center',

        paddingVertical:10,
        // borderWidth:1,
    },
    headerText:{
        textAlign: 'center',
        color:colors.dark,
        fontFamily:'Rubik-Medium',
        fontSize: 18
    },
    // title:{
    //     width:'85%',
    //     color:colors.white,
    //     fontFamily:'Rubik-Medium',
    //     fontSize:18,
    //     // borderWidth:1,
    //     textAlign:'center',
    // },
    text:{
        textAlign:'center',
        fontSize:24,

    },
    wrapper: {
        height: "100%",
        width:'100%',
        // alignItems: 'center',
    },
    brand:{
        // paddingTop:"30%",

        backgroundColor:'grey',
        width:'100%',
        height:'100%',
        // overflow: 'hidden',
        // blurRadius:6
        // borderWidth:1,

    },
    // text:{
    //     textAlign:'center',
    //     fontSize:18,
    //     color:colors.white,
    //     paddingTop:17,
    //     width:110,
    //     fontFamily:'Rubik-Bold',
    //     // borderWidth:1
    // },
    text1:{
        color:colors.white,
        fontSize: 35,
        width:224,
        textAlign:'center',
        // fontWeight:'bold',
        fontFamily:'Rubik-Medium',
        paddingTop: '25%',
        // backgroundColor: 'transparent'
        // borderWidth:1,
    },
    backButton:{
        // alignItems:'center'
        paddingLeft:20,
    },
    view:{
        flex:1,
        // marginTop:-Height/1.2,
        // paddingTop:40,
        // paddingHorizontal:30,
        zIndex:1,
        width:'100%',
        // height:205,
        backgroundColor: colors.white,
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        // borderWidth:1,
    },
    footer:{
        textAlign:'center',
        // position:'absolute',
        // bottom:10,
        // left:'45%',
        // paddingTop:0.07*Height,
        fontFamily:'Rubik-Regular',
        color:colors.footerColor
    },
    row:{
        // borderWidth:1,
        paddingLeft:10,
        paddingVertical:10,
        // paddingTop:50,
        // width:'100%',
        flexDirection:'row',
        backgroundColor:'transparent',
        alignItems:'center',
        // borderWidth:1,
        paddingHorizontal:10,

    },
    rows:{
        // paddingLeft:10,
        paddingVertical:10,
        // paddingTop:50,
        // justifyContent:'center',
        width:'100%',
        flexDirection:'row',
        backgroundColor:'transparent',
        alignItems:'center'
    },
    checkLabel:{
        // width:'90%',
        fontSize:14,
        paddingLeft:10,
        fontFamily:'Rubik-Regular',
        color:colors.grey
    },
    title:{
        width:'85%',
        color:colors.white,
        fontFamily:'Rubik-Medium',
        fontSize:18,
        // borderWidth:1,
        textAlign:'center',
    },
    touchable:{
        // borderWidth:1,
        paddingHorizontal:30,
        marginRight:10,

    },
    wrappers: {
        // flex:1,
        flexGrow:1,
        paddingVertical: 16,
        paddingTop:40,
        paddingHorizontal:30,
    },
    error:{
        color:'#e00a13',
        fontSize: 12,
        paddingBottom:15,
        // marginHorizontal:'9%'
    },
    row1:{
        // borderWidth:1,
        paddingBottom: '8%',
        paddingLeft:10,
        paddingVertical:10,
        paddingTop:50,
        width:'100%',
        flexDirection:'row',
        backgroundColor:'transparent',
        alignItems:'center'
    },

})

export default styles
