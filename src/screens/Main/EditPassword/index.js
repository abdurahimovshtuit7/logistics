import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ScrollView, ImageBackground, Dimensions,
} from "react-native";
// import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import BackButton from "../../../assets/image/SVG/BackButton";
import LinearGradient from "react-native-linear-gradient";

// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

class Profile extends Component {
    componentDidMount() {
        this.aboutMe()
    }
    aboutMe() {
        // this.setState({isFetched, refreshing})
        Routines.auth.aboutMe({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    // this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch((e) => {
                console.log('Error:',e)
                let status = get(e,'message.error','Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }

            })
    }
    render() {
        const {handleSubmit,errors,t,aboutOfMe} = this.props
       console.log('aboutOfMe:',aboutOfMe)
        const fullname = get(aboutOfMe,'data.fullname','')
        return (
            <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                            style={styles.wrapper}>
            <View style={styles.container}>

                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>

                  <View style={styles.header}>
                      <TouchableOpacity style={styles.touchable}  onPress={() => {
                          NavigationService.goBack()
                      }}>
                          <BackButton style={styles.backButton} color={colors.black}/>
                      </TouchableOpacity>
                      <Text style={styles.headerText}>{t('password')}</Text>
                  </View>

                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrappers}>
                    <Components.Layout>
                        {/*<Field*/}
                        {/*    name={'name'}*/}
                        {/*    label={'name'}*/}
                        {/*    // title={'Ваше Ф.И.О *'}*/}
                        {/*    placeHolder={fullname}*/}
                        {/*    component={Components.Input}*/}
                        {/*    // mask={"(+998) [00] [000] [00] [00]"}*/}
                        {/*/>*/}
                        <Field
                            name={'password'}
                            label={'password'}
                            // title={'Ваше Ф.И.О *'}
                            placeHolder={t('oldPassword')}
                            component={Components.Input}
                            // mask={"(+998) [00] [000] [00] [00]"}
                        />
                        <Field
                            name={'newpassword'}
                            label={'newpassword'}
                            // title={'Ваше Ф.И.О *'}
                            placeHolder={t('newPassword')}
                            component={Components.Input}
                            // mask={"(+998) [00] [000] [00] [00]"}
                        />
                        <Components.Button text={t('done')}
                                           color={colors.white}
                                           background={colors.green}
                                           padding = {Height*0.1}
                                           touch={() => {
                                               handleSubmit()
                                               // NavigationService.goBack()
                                           }}
                                           isActive={this.props.isSubmitting}
                        />
                    </Components.Layout>

                </KeyboardAwareScrollView>

            </View>
            </LinearGradient>
        )
    }
}

Profile = withFormik({
    mapPropsToValues: (props) => ({
        // name: get(props.aboutOfMe,'data.fullname',''),
        password: "",
        newpassword:''

    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            // name: Yup.string().required(t("required")),
            // password: Yup.string().required(t("required")),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);
        // NavigationService.navigate('admin')


        let {name, phone,email,password,newpassword,confirmPassword} = values;
        // let {route} = this.props
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)
        setSubmitting(true);
        Routines.auth.updateDetails({
            request: {
                data: {
                    // fullname: name,
                    currentPassword: password,
                    newPassword: newpassword,

                }
            }
        }, props.dispatch)
            .then((data) => {
                setSubmitting(false);

                // NavigationService.goBack()
                // if(get(data.response,'data.success',false)){
                //     // NavigationService.navigate('home')
                //     // this.aboutMe()
                //     return (
                        Components.Toast.show("Sizning parolingiz o'zgartrildi!")
                //     )
                //
                // } else {
                //     console.log("Success")
                // }
            })
            .catch(e => {
                console.log('Error:',e)
                let status = get(e,'message.error','Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                setSubmitting(false);
            });
    }
})(Profile);
const mapStateToProps = (state, ownProps) => {
    return {
          aboutOfMe:state.profile.aboutOfMe
    };
};
Profile = connect(mapStateToProps)(Profile)
export default withTranslation('main')(Profile)
