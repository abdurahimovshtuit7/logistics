import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ScrollView,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import Menu from "../../../assets/image/SVG/Menu";
import LinearGradient from "react-native-linear-gradient";
import Icon from 'react-native-vector-icons/MaterialIcons'
const time = [
    {label: '1 соатда бўшайман',value:'1'},
    {label: '2 соатда бўшайман',value:'2'},
    {label: '3 соатда бўшайман',value:'3'},
    {label: '4 соатда бўшайман',value:'4'},
    {label: '5 соатда бўшайман',value:'5'},
    {label: '6 соатда бўшайман',value:'6'},
    {label: '7 соатда бўшайман',value:'7'},
    {label: '8 соатда бўшайман',value:'8'},
    {label: '9 соатда бўшайман',value:'9'},
    {label: '10 соатда бўшайман',value:'10'},
    {label: '11 соатда бўшайман',value:'11'},
    {label: '12 соатда бўшайман',value:'12'},
    {label: '13 соатда бўшайман',value:'13'},
    {label: '14 соатда бўшайман',value:'14'},
    {label: '15 соатда бўшайман',value:'15'},
    {label: '16 соатда бўшайман',value:'16'},
    {label: '17 соатда бўшайман',value:'17'},
    {label: '18 соатда бўшайман',value:'18'},
    {label: '19 соатда бўшайман',value:'19'},
    {label: '20 соатда бўшайман',value:'20'},
    {label: '21 соатда бўшайман',value:'21'},
    {label: '22 соатда бўшайман',value:'22'},
    {label: '23 соатда бўшайман',value:'23'},
    {label: '24 соатда бўшайман',value:'24'},
]
const timeRu = [
    {label: 'Я буду свободен через 1 час',value:'1'},
    {label: 'Я буду свободен через 2 час',value:'2'},
    {label: 'Я буду свободен через 3 час',value:'3'},
    {label: 'Я буду свободен через 4 час',value:'4'},
    {label: 'Я буду свободен через 5 час',value:'5'},
    {label: 'Я буду свободен через 6 час',value:'6'},
    {label: 'Я буду свободен через 7 час',value:'7'},
    {label: 'Я буду свободен через 8 час',value:'8'},
    {label: 'Я буду свободен через 9 час',value:'9'},
    {label: 'Я буду свободен через 10 час',value:'10'},
    {label: 'Я буду свободен через 11 час',value:'11'},
    {label: 'Я буду свободен через 12 час',value:'12'},
    {label: 'Я буду свободен через 13 час',value:'13'},
    {label: 'Я буду свободен через 14 час',value:'14'},
    {label: 'Я буду свободен через 15 час',value:'15'},
    {label: 'Я буду свободен через 16 час',value:'16'},
    {label: 'Я буду свободен через 17 час',value:'17'},
    {label: 'Я буду свободен через 18 час',value:'18'},
    {label: 'Я буду свободен через 19 час',value:'19'},
    {label: 'Я буду свободен через 20 час',value:'20'},
    {label: 'Я буду свободен через 21 час',value:'21'},
    {label: 'Я буду свободен через 22 час',value:'22'},
    {label: 'Я буду свободен через 23 час',value:'23'},
    {label: 'Я буду свободен через 24 час',value:'24'},
]

class Select extends Component {

    state = {
        isSpinnerActive:false,
        item:''
    }

    navigatorScreen = (item) => {
        const {route} = this.props
        if(get(route,'params.time','')===2){
            NavigationService.navigate("selectTime", {
                data: this.selectTime(),
                item:item
            });
        }
        else if(get(route,'params.time','')===1){
            this.setState({item:item,isSpinnerActive:true})

            Routines.auth.updateStatus({
                request: {
                    data: {
                        status: 'Свободен',
                        location: item,
                        timeOfStatus: this.props.t('free')
                    }
                }
            }, this.props.dispatch)
                .then((data) => {
                    this.setState({isSpinnerActive:false})
                    NavigationService.goBack()

                })
                .catch(e => {

                    console.log('Error:', e)
                    let status = get(e, 'message.error', 'Oops... something went wrong!')
                    if (e.message === 'NETWORK_ERROR') {
                        Components.Toast.show("Tarmoqqa ulanmagansiz")
                    } else {
                        Components.Toast.show(status)
                    }
                    this.setState({isSpinnerActive:false})

                });

            // NavigationService.navigate('mainPage', {
            //
            // })
        }

    }
    selectTime (){
        if(this.props.currentLangCode==='uz'){
            return time
        } else return timeRu
    }


    render() {
        const {route,currentLangCode,t} = this.props
        let {onSelect = ()=>{}} = get(route,'params',{})
        console.log("DATA:",route.params.data)
        const item ='00:00'
        return (
            <>
                <View style={styles.row}>
                    <Text style={styles.title}>{t('joylashgan_joyingiz')}</Text>
                    <TouchableOpacity style={styles.touchable} onPress={() => {
                        // NavigationService.navigate("mainPage", {
                        //     screenName: "select",
                        //     onSelect: ()=>{ console.warn("ABC") }
                        // });
                        NavigationService.goBack()
                    }}>
                        <Icon name={'cancel'} style={styles.backButton} size={35} color={colors.darkBlue}/>
                    </TouchableOpacity>
                </View>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                 <ScrollView>
                     {
                         get(route,'params.data',[]).map((item,index)=>{
                            return (
                                <Components.Select text={item.label}
                                                   key={index}
                                                   color={colors.white}
                                                   background={colors.darkBlue}
                                                   isActive = {this.state.item === item.label?this.state.isSpinnerActive:null}
                                    // padding = {'110%'}
                                                   paddingHorizantal = {20}
                                                   touch={() => {
                                                       // handleSubmit()
                                                       onSelect(item.label)
                                                       this.navigatorScreen(item.label)
                                                       // NavigationService.navigate('mainPage', {
                                                       //
                                                       // })
                                                   }
                                                   }
                                />
                            )
                         })
                     }

                 </ScrollView>


            </>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode: state.language.lang,

    };
};
Select = connect(mapStateToProps)(Select)
export default withTranslation('main')(Select)
