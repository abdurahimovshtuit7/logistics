import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {statusBarHeigth} from "../../../assets/styles/commons";

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:colors.blue

    },
    text:{
        textAlign:'center',
        fontSize:24,

    },
    touchable:{
        // borderWidth:1,
        // paddingHorizontal:10,
        // paddingVertical: 8,
        // position:'absolute',
        // right:25,
        // top:50
    },
    backButton:{
        alignItems:'center',
        paddingLeft:10,
        justifyContent: 'flex-end'
    },
    row:{
        // borderWidth:1,
        // paddingLeft:10,
        paddingBottom:20,
        paddingTop:statusBarHeigth,
        width:'100%',
        flexDirection:'row',
        // backgroundColor:colors.blue,
        alignItems:'center'
    },
    title:{
        textAlign: 'center',
        // paddingTop: 10,
        // borderWidth:1,
        width: '80%',
        paddingHorizontal:10,
        fontFamily:'Rubik-Bold',
        fontSize: 22,
        color:colors.red,

    }

})

export default styles
