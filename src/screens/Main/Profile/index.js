import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ScrollView, ImageBackground, Dimensions,
} from "react-native";
// import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import BackButton from "../../../assets/image/SVG/BackButton";
import LinearGradient from "react-native-linear-gradient";
import Translate from "../../../services/translateTitle";
import * as Animatable from "react-native-animatable";
import Animated from "react-native-reanimated";
import ImagePicker from "react-native-image-crop-picker";
import {image, tex_pass} from "../../../services/actions";
import BottomSheet from "reanimated-bottom-sheet";

// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

class Profile extends Component {
    componentDidMount() {
        this.aboutMe()
        this.PickerData()
    }

    aboutMe() {
        // this.setState({isFetched, refreshing})
        Routines.auth.aboutMe({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    // this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch((e) => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }

            })
    }

    PickerData = (id) => {
        // this.setState({search});
        Routines.auth.getPickerData({
            request: {},
        }, this.props.dispatch)
            .then((data) => {
                    // this.getItems(1)
                    // NavigationService.goBack()
                    this.props.setFieldValue('id', get(this.props.aboutOfMe, 'data._id', ''))
                },
            )
            .catch(e => {
                // if(e.message === 'NETWORK_ERROR'){
                //     ToastAndroid.show(changeLanguage['tarmoq'], ToastAndroid.SHORT)
                //     // NavigationService.goBack()
                // }
            })
    };
    bs = React.createRef();
    ts = React.createRef();
    fall = new Animated.Value(1);
    takePhotoFromCamera = () => {
        ImagePicker.openCamera({
            // compressImageMaxWidth: 300,
            // compressImageMaxHeight: 300,
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(image(file));
            this.props.setFieldValue('file', file)
            this.bs.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(image({}));
        });
    }
    choosePhotoFromLibrary = () => {
        ImagePicker.openPicker({
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(image(file));
            this.props.setFieldValue('file', file)
            // this.setState({file: image})
            this.bs.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(image({}));
        });

    }
    takePhotoFrom = () => {
        ImagePicker.openCamera({
            // compressImageMaxWidth: 300,
            // compressImageMaxHeight: 300,
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(tex_pass(file));
            this.props.setFieldValue('tex_pass', file)
            this.ts.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(tex_pass({}));
        });
    }
    choosePhotoFromLib = () => {
        ImagePicker.openPicker({
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(tex_pass(file));
            this.props.setFieldValue('tex_pass', file)
            // this.setState({file: image})
            this.ts.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(tex_pass({}));
        });

    }

    renderInner = () => (
        <View style={styles.panel}>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.panelTitle}>Upload Photo</Text>
                <Text style={styles.panelSubtitle}/>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={this.takePhotoFromCamera}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={this.choosePhotoFromLibrary}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => this.bs.current.snapTo(1)}>
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );
    renderIn = () => (
        <View style={styles.panel}>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.panelTitle}>Upload Photo</Text>
                <Text style={styles.panelSubtitle}/>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={this.takePhotoFrom}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={this.choosePhotoFromLib}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => this.ts.current.snapTo(1)}>
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );

    renderHeader = () => (
        <View style={styles.header}>
            <View style={styles.panelHeader}>
                {/*<View style={styles.panelHandle}/>*/}
            </View>
        </View>
    );


    render() {
        const {handleSubmit, errors, t, aboutOfMe, image, tex_pass, getPickerData, currentLangCode, route} = this.props
        console.log('aboutOfMe:', aboutOfMe)
        const fullname = get(aboutOfMe, 'data.fullname', '')
        return (
            <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                            style={styles.wrapper}>
                <BottomSheet
                    ref={this.bs}
                    snapPoints={['43%', 0]}
                    renderContent={this.renderInner}
                    renderHeader={this.renderHeader}
                    initialSnap={1}
                    callbackNode={this.fall}
                    enabledGestureInteraction={true}
                    enabledInnerScrolling={false}
                    callbackThreshold={0.9}
                />
                <BottomSheet
                    ref={this.ts}
                    snapPoints={['43%', 0]}
                    renderContent={this.renderIn}
                    renderHeader={this.renderHeader}
                    initialSnap={1}
                    callbackNode={this.fall}
                    enabledGestureInteraction={true}
                    enabledInnerScrolling={false}
                    callbackThreshold={0.9}
                />
                <View style={styles.container}>


                    <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>

                    <View style={styles.header}>
                        <TouchableOpacity style={styles.touchable} onPress={() => {
                            NavigationService.goBack()
                        }}>
                            <BackButton style={styles.backButton} color={colors.black}/>
                        </TouchableOpacity>
                        <Text style={styles.headerText}>{t('edit')}</Text>
                    </View>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                             contentContainerStyle={styles.wrappers}>
                        <Field
                            name={'name'}
                            label={'name'}
                            title={t('FIO')}
                            color={colors.lightBlack}
                            placeHolder={get(aboutOfMe, 'data.fullname', '')}
                            component={Components.InputText}

                        />
                        {errors.name ? (
                            <Text style={styles.error}>{errors.name}</Text>
                        ) : null}
                        <Field
                            name={'phone'}
                            label={'phone'}
                            title={t('phone')}
                            color={colors.lightBlack}
                            // placeHolder={get(aboutOfMe,'data.phonenumber','')}
                            component={Components.InputText}
                            placeHolder={`+${get(aboutOfMe, 'data.phonenumber', '')}`}
                            mask={"(+998) [00] [000] [00] [00]"}
                            keyboards={'number-pad'}
                        />
                        {errors.name ? (
                            <Text style={styles.error}>{errors.name}</Text>
                        ) : null}
                        <Components.Picker title={t('car_type')}
                                           zIndex={5000}
                                           items={Translate.title(currentLangCode, getPickerData.avtoType_ru, getPickerData.avtoType_uz)}
                                           placeholder={get(aboutOfMe, 'data.transportType', 'Car')}
                                           select={(value) => this.props.setFieldValue('carType', value)}


                        />
                        {errors.carType ? (
                            <Text style={styles.error}>{errors.carType}</Text>
                        ) : null}


                        <Field
                            name={'number'}
                            label={'number'}
                            title={t('transport_nomer')}
                            color={colors.lightBlack}
                            placeHolder={get(aboutOfMe, 'data.transportGovNumber', '')}
                            // mask={"[00] [000] [AAA]"}
                            component={Components.InputText}
                        />
                        {errors.number ? (
                            <Text style={styles.error}>{errors.number}</Text>
                        ) : null}

                        <Field
                            name={'volume'}
                            label={'volume'}
                            title={t('volume')}
                            color={colors.lightBlack}
                            placeHolder={get(aboutOfMe, 'data.baggageVolume', '').toString()}
                            component={Components.InputText}
                            keyboards={'number-pad'}
                        />
                        {errors.volume ? (
                            <Text style={styles.error}>{errors.volume}</Text>
                        ) : null}

                        <Field
                            name={'mass'}
                            label={'mass'}
                            title={t('mass')}
                            color={colors.lightBlack}
                            placeHolder={get(aboutOfMe, 'data.baggageMass', '').toString()}
                            component={Components.InputText}
                            keyboards={'number-pad'}
                        />
                        {errors.mass ? (
                            <Text style={styles.error}>{errors.mass}</Text>
                        ) : null}

                        <Components.DownloadButton text={t('passport')}
                                                   color={colors.text}
                                                   background={colors.white}
                                                   border={1}
                                                   touch={() => {
                                                       this.bs.current.snapTo(0)
                                                   }}
                        />
                        {errors.file ? (
                            <Text style={styles.error}>{errors.file}</Text>
                        ) : null}
                        <Components.DownloadButton text={t('tex_passport')}
                                                   color={colors.text}
                                                   background={colors.white}
                                                   border={1}
                                                   touch={() => {
                                                       // this.props.setFieldValue('file', image)
                                                       this.ts.current.snapTo(0)
                                                   }}
                        />

                        <Components.Button text={t('done')}
                                           color={colors.white}
                                           background={colors.green}
                                           padding={'5%'}
                                           touch={() => {
                                               handleSubmit()
                                           }}
                                           isActive={this.props.isSubmitting}

                        />
                        <Components.Button text={t('password')}
                                           color={colors.darkBlue}
                                           background={colors.white}
                                           padding={15}
                                           border={1}
                                           touch={() => {
                                               NavigationService.navigate('editPassword')
                                           }}
                                           isActive={this.props.isSubmitting}

                        />
                    </KeyboardAwareScrollView>


                </View>
            </LinearGradient>
        )
    }
}

Profile = withFormik({
    mapPropsToValues: (props) => ({
        name: get(props.aboutOfMe, 'data.fullname', ''),
        volume: get(props.aboutOfMe, 'data.baggageVolume', ''),
        mass: get(props.aboutOfMe, 'data.baggageMass', ''),
        // transportType: "",
        file: false,
        // isChecked: true,
        tex_pass: false,
        number: get(props.aboutOfMe, 'data.transportGovNumber', ''),
        carType: get(props.aboutOfMe, 'data.transportType', ''),
        phone: get(props.aboutOfMe, 'data.phonenumber', ''),
        id: ''
        // oneID: '',
        // location:''

    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            // name: Yup.string().required(t("required")),
            // password: Yup.string().required(t("required")),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        setSubmitting(true);
        console.log(values);

        let {name, phone, mass, file, tex_pass, id, carType, volume, number} = values;
        // let {route} = this.props
        let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)

        let formData = new FormData()
        console.log("FORMDATA:", file === {})
        if (file || tex_pass) {
            if (file && tex_pass) {
                const pass = tex_pass.path.split('/');
                formData.append(`techPassportPhoto`, {
                    name: tex_pass.name ? tex_pass.name : pass[pass.length - 1],
                    filename: tex_pass.filename ? tex_pass.filename : pass[pass.length - 1],
                    type: tex_pass.type ? tex_pass.type : tex_pass.mime,
                    uri: tex_pass.sourceURL ? tex_pass.sourceURL : tex_pass.path
                });
                const parts = file.path.split('/');
                formData.append(`passportPhoto`, {
                    name: file.name ? file.name : parts[parts.length - 1],
                    filename: file.filename ? file.filename : parts[parts.length - 1],
                    type: file.type ? file.type : file.mime,
                    uri: file.sourceURL ? file.sourceURL : file.path
                });
            } else if (tex_pass) {
                const pass = tex_pass.path.split('/');
                formData.append(`techPassportPhoto`, {
                    name: tex_pass.name ? tex_pass.name : pass[pass.length - 1],
                    filename: tex_pass.filename ? tex_pass.filename : pass[pass.length - 1],
                    type: tex_pass.type ? tex_pass.type : tex_pass.mime,
                    uri: tex_pass.sourceURL ? tex_pass.sourceURL : tex_pass.path
                });
            } else if (file) {
                const parts = file.path.split('/');
                formData.append(`passportPhoto`, {
                    name: file.name ? file.name : parts[parts.length - 1],
                    filename: file.filename ? file.filename : parts[parts.length - 1],
                    type: file.type ? file.type : file.mime,
                    uri: file.sourceURL ? file.sourceURL : file.path
                });
            }
        }

        formData.append("fullname", name)
        formData.append("phonenumber", onlyNums)
        // formData.append("password", password)
        formData.append("transportType", carType)
        formData.append("transportGovNumber", number)
        formData.append("baggageVolume", volume)
        formData.append("baggageMass", mass)

        Routines.auth.updateProfile({
            request: {
                data: formData
            }
        }, props.dispatch)
            .then((data) => {
                setSubmitting(false);
                Components.Toast.show("Sizning ma'lumotlaringiz o'zgartrildi!")
            })
            .catch(e => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                setSubmitting(false);
            });
    }
})(Profile);
const mapStateToProps = (state, ownProps) => {
    return {
        aboutOfMe: state.profile.aboutOfMe,
        file: state.file,
        image: state.user.image,
        tex_pass: state.user.tex_pass,
        currentLangCode: state.language.lang,
        getPickerData: state.profile.getPickerData,
    };
};
Profile = connect(mapStateToProps)(Profile)
export default withTranslation('main')(Profile)
