import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput,
    ImageBackground,
    ScrollView,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get, toArray} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

// -----SVG----
import Menu from 'assets/image/SVG/Menu'
import LinearGradient from "react-native-linear-gradient";
import BackButton from "../../../assets/image/SVG/BackButton";
import BottomSheet from "reanimated-bottom-sheet";
import {saveUser} from "../../../services/actions";
import Translate from "../../../services/translateTitle";
//
// const time = [
//     {label: '1 соатда бўшайман',value:'1'},
//     {label: '2 соатда бўшайман',value:'2'},
//     {label: '3 соатда бўшайман',value:'3'},
//     {label: '4 соатда бўшайман',value:'4'},
//     {label: '5 соатда бўшайман',value:'5'},
//     {label: '6 соатда бўшайман',value:'6'},
//     {label: '7 соатда бўшайман',value:'7'},
//     {label: '8 соатда бўшайман',value:'8'},
//     {label: '9 соатда бўшайман',value:'9'},
//     {label: '10 соатда бўшайман',value:'10'},
//     {label: '11 соатда бўшайман',value:'11'},
//     {label: '12 соатда бўшайман',value:'12'},
//     {label: '13 соатда бўшайман',value:'13'},
//     {label: '14 соатда бўшайман',value:'14'},
//     {label: '15 соатда бўшайман',value:'15'},
//     {label: '16 соатда бўшайман',value:'16'},
// ]
// const timeRu = [
//     {label: 'Я буду свободен через 1 час',value:'1'},
//     {label: 'Я буду свободен через 2 час',value:'2'},
//     {label: 'Я буду свободен через 3 час',value:'3'},
//     {label: 'Я буду свободен через 4 час',value:'4'},
//     {label: 'Я буду свободен через 5 час',value:'5'},
//     {label: 'Я буду свободен через 6 час',value:'6'},
//     {label: 'Я буду свободен через 7 час',value:'7'},
//     {label: 'Я буду свободен через 8 час',value:'8'},
//     {label: 'Я буду свободен через 9 час',value:'9'},
//     {label: 'Я буду свободен через 10 час',value:'10'},
//     {label: 'Я буду свободен через 11 час',value:'11'},
//     {label: 'Я буду свободен через 12 час',value:'12'},
//     {label: 'Я буду свободен через 13 час',value:'13'},
//     {label: 'Я буду свободен через 14 час',value:'14'},
//     {label: 'Я буду свободен через 15 час',value:'15'},
//     {label: 'Я буду свободен через 16 час',value:'16'},
// ]

class MainPage extends Component {
    state = {
        selected: '',
    }

    componentDidMount() {
        this.PickerData()
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.aboutMe()
        });


        // this.status()
        this.props.setFieldValue("location", Translate.title(this.props.currentLangCode, this.objectToArray(this.props.getPickerData.location_ru), this.objectToArray(this.props.getPickerData.location_uz))[0].label)
    }

    componentWillUnmount() {
        this._unsubscribe();
    }


    Selected = (id) => {
        const {currentLangCode, getPickerData} = this.props
        NavigationService.navigate("select", {
            data: Translate.title(currentLangCode, this.objectToArray(getPickerData.location_ru), this.objectToArray(getPickerData.location_uz)),
            onSelect: (item) => {
                this.props.setFieldValue("location", item)
            },
            time: id
        });
        // let {location, name} = this.props.values
        // let {t, handleSubmit, setFieldValue, values, setSubmitting} = this.props
        // this.setState({selected: id});
        // console.log("ID", id)
        // if (id === '1') {
        //     setFieldValue('name', t('free'))
        //     setFieldValue('id', 'Свободен')
        //     setTimeout(() => {
        //         handleSubmit()
        //     }, 500);
        //
        // } else {
        //     if (name === t('free') || name === 'Vaqtni kiriting') {
        //         setFieldValue('name', 'Vaqtni kiriting')
        //         Components.Toast.show("Vaqtni kiriting")
        //     } else {
        //         setFieldValue('id', 'Занят')
        //         setTimeout(() => {
        //             handleSubmit()
        //         }, 500);
        //
        //     }
        // }

    }
    // selectTime (){
    //     if(this.props.currentLangCode==='uz'){
    //        return time
    //     } else return timeRu
    // }

    aboutMe() {
        // this.setState({isFetched, refreshing})
        let {t, handleSubmit, setFieldValue, setSubmitting} = this.props

        Routines.auth.aboutMe({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    setFieldValue('location', get(this.props.aboutOfMe, 'data.location', ''))
                    // this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch((e) => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }

            })
    }


    PickerData() {
        Routines.auth.getPickerData({
            request: {},
        }, this.props.dispatch)
            .then((data) => {
                },
            )
            .catch(e => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }

            })
    };

    objectToArray = (object) => {
        const array = toArray(object)
        const obj = [{
            label: '',
            value: ''
        }]
        array.map((item, index) => {
            // console.log("b", item)
            obj[index] = {
                label: item,
                value: item
            }
        })
        return obj
    }

    render() {
        const PROP = [
            {
                key: '1',
                title: this.props.t('free'),
                color: colors.brandGreen
                // svg: <Person/>,
            },
            {
                key: '2',
                title: this.props.t('busy'),
                color: colors.red
                // svg: <HandShake/>,
            },
        ];

        const {navigation, route, t, aboutOfMe, handleSubmit, setFieldValue, values, currentLangCode, getPickerData} = this.props
        let {item1} = this.state
        console.log('aboutOfMe:', get(aboutOfMe, 'data', ''))

        return (
            <>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>

                <ImageBackground
                    // source={require('assets/image/tracker.png')}
                    style={styles.brand}
                    resizeMode={'cover'}
                    blurRadius={1}
                >
                    <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                    style={styles.wrapper}>

                        <View style={styles.row}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                navigation.toggleDrawer()
                            }}>
                                <Menu style={styles.backButton}/>
                            </TouchableOpacity>
                        </View>
                        <ScrollView contentContainerStyle={styles.scroll}>
                            <View style={styles.contentView}>

                                {/*{*/}
                                {/*    get(aboutOfMe, 'data.status', '') === 'Свободен'?(null):(*/}
                                {/*        <Components.StatusButton text1={t('active')}*/}
                                {/*                                 text2={get(aboutOfMe,'data.timeOfStatus','')}*/}
                                {/*                                 placeholder={t('free')}*/}
                                {/*                                 onSelected={() => {*/}
                                {/*                                     // NavigationService.navigate("select", {*/}
                                {/*                                     //     data: this.selectTime(),*/}
                                {/*                                     //     onSelect: (item) => {*/}
                                {/*                                     //         console.log("BLABLABLA..............")*/}
                                {/*                                     //         // this.props.setFieldValue("name", item)*/}
                                {/*                                     //     }*/}
                                {/*                                     // });*/}
                                {/*                                 }}*/}
                                {/*        />*/}
                                {/*    )*/}
                                {/*}*/}

                                {/*<Components.StatusButton text1={t('location')}*/}
                                {/*                         text2={get(aboutOfMe,'data.location','')}*/}
                                {/*                         zIndex={4000}*/}
                                {/*                         items={item1}*/}
                                {/*                         placeholder={get(aboutOfMe, 'data.location', '')}*/}
                                {/*                         onSelected={() => {*/}
                                {/*                             // NavigationService.navigate("select", {*/}
                                {/*                             //     data: Translate.title(currentLangCode, this.objectToArray(getPickerData.location_ru), this.objectToArray(getPickerData.location_uz)),*/}
                                {/*                             //     onSelect: (item) => {*/}
                                {/*                             //         this.props.setFieldValue("location", item)*/}
                                {/*                             //     }*/}
                                {/*                             // });*/}
                                {/*                         }}*/}
                                {/*/>*/}

                                <Text style={styles.status}>
                                    {t('status')}
                                </Text>
                                <Components.AnimationButton PROP={PROP}
                                                            margin={'8%'}
                                                            id={get(aboutOfMe, 'data.status', '') === 'Свободен' ? true : false}
                                                            title={t('free')}
                                                            color={colors.brandGreen}
                                                            status={get(aboutOfMe, 'data.status', '')}
                                                            isActive={this.props.isSubmitting}
                                                            onSelected={() => {
                                                                this.Selected(1)
                                                                // handleSubmit()
                                                            }}

                                />
                                <Components.AnimationButton PROP={PROP}
                                    // margin={'8%'}
                                                            id={get(aboutOfMe, 'data.status', '') === 'Свободен' ? false : true}
                                                            title={t('busy')}
                                                            color={colors.red}
                                                            status={get(aboutOfMe, 'data.status', '')}
                                                            isActive={this.props.isSubmitting}
                                                            onSelected={() => {
                                                                this.Selected(2)
                                                                // handleSubmit()
                                                            }}

                                />
                            </View>
                        </ScrollView>

                    </LinearGradient>
                </ImageBackground>
            </>
        )
    }
}

MainPage = withFormik({
    mapPropsToValues: (props) => ({
        name: "",
        location: get(props.aboutOfMe, 'data.location', ''),
        id: ''
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            // name: Yup.string().required(t("required")),
            location: Yup.string().required(t("required")),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log('handleSubmit_Values:', values);
        let {name, location, id} = values;

        setSubmitting(true);

        Routines.auth.updateStatus({
            request: {
                data: {
                    status: id,
                    location: location,
                    timeOfStatus: name
                }
            }
        }, props.dispatch)
            .then((data) => {
                setSubmitting(false);

            })
            .catch(e => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                setSubmitting(false);
            });
    }
})(MainPage);
const mapStateToProps = (state, ownProps) => {
    return {
        getPickerData: state.profile.getPickerData,
        currentLangCode: state.language.lang,
        aboutOfMe: state.profile.aboutOfMe

    };
};
MainPage = connect(mapStateToProps)(MainPage)
export default withTranslation('main')(MainPage)
