import {Dimensions, StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;
import {horizontal,statusBarHeigth} from "assets/styles/commons";


const styles = StyleSheet.create({
    wrapper: {
        flex:1,
        // height: "100%",
        // width:'100%',
        // alignItems: 'center',
    },
    brand:{
        // paddingTop:"30%",
        flex:1,
        backgroundColor: colors.darkBlue,

        // backgroundColor:'#606da0',
        // width:'100%',
        // height:'100%',
        // overflow: 'hidden',
        // blurRadius:6
        // borderWidth:1,
    },
    scroll:{
        // paddingHorizontal: 20,
         paddingVertical: 20,
        // marginHorizontal:20,
    },
    row:{
        // borderWidth:1,
        // paddingLeft:10,
        // paddingVertical:10,
        paddingTop:statusBarHeigth,
        width:'100%',
        flexDirection:'row',
        backgroundColor:'transparent',
        alignItems:'center'
    },
    touchable:{
        // borderWidth:1,
        paddingHorizontal:20,
        paddingVertical:16
    },
    backButton:{
        // alignItems:'center'
        paddingLeft:20,
    },
    contentView:{
      // paddingTop: 20,
    },
    status:{
        color:colors.white,
        textAlign:'center',
        marginTop:'24%',
        fontFamily:'Rubik-Regular',
        fontSize:20,
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10,
    },
    panel: {
        // flex:1,
        padding: 20,
        paddingBottom:54,
        // zIndex: 2,
        backgroundColor: '#FFFFFF',
        paddingTop: 20,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        // borderWidth:1,
        // borderColor:colors.green,
        shadowColor: '#000000',
        shadowOffset: {width: 0, height: 0},
        shadowRadius: 15,
        shadowOpacity: 0.3,
        elevation:6,
    },
    panelTitle: {
        fontSize: 27,
        height: 35,
    },
    panelSubtitle: {
        fontSize: 14,
        color: 'gray',
        height: 30,
        marginBottom: 10,
    },
    panelButton: {
        padding: 13,
        borderRadius: 10,
        backgroundColor:colors.green,
        alignItems: 'center',
        marginVertical: 7,
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    },

})

export default styles
