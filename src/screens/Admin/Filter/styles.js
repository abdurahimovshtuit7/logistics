import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {statusBarHeigth} from 'assets/styles/commons'


const styles = StyleSheet.create({
    container:{
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor:colors.darkBlue,
        paddingTop:statusBarHeigth,
        // borderWidth: 1,


    },
    row:{
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    backButton:{
        paddingLeft:20,
    },
    touchable:{
        paddingHorizontal:10,
    },
    filter:{
        // borderWidth:1,
        paddingVertical:10,
        paddingHorizontal:30
    },
    scroll:{
        paddingVertical:8,
        paddingHorizontal:20,
        paddingBottom:20,
    },
    searchContainer:{
        // marginTop:50,
        // marginLeft:37,
        // marginRight:20,
        marginHorizontal:20,
        marginVertical: 10,
        padding:0,

        backgroundColor:'transparent',
        // borderWidth:1,
        borderRadius:5,
        borderColor:'transparent'
    },
    input:{
        backgroundColor:'#fff',
        // borderWidth: 1,
        padding:0,
        // marginVertical:5
    },
    text:{
        textAlign:'center',
        fontSize:24,

    },
    title:{
        color:colors.white,
        justifyContent:'center',
        textAlign: 'center',
        fontFamily:'Rubik-Medium',
        fontSize: 18,
        paddingBottom: 10,
        width: '70%'

    },
    mass:{
        color:colors.white,
        fontSize:16,
        fontFamily: 'Rubik-Italic'
    },
    TextInput:{
        // height: 50,
        paddingVertical:18,
        paddingHorizontal:15,
        // borderColor: 'gray',
        // borderWidth: 1,
        borderRadius:5,
        marginBottom:20,
        backgroundColor:colors.white
    },
    wrapper: {
        height: "100%",
        width:'100%',
        // alignItems: 'center',
    },
    row1:{
        // borderWidth:1,
        paddingBottom: '8%',
        paddingLeft:10,
        paddingVertical:10,
        // paddingTop:50,
        width:'100%',
        flexDirection:'row',
        backgroundColor:'transparent',
        alignItems:'center'
    },
    infinite:{
        paddingVertical:8,
        paddingBottom:20,
    },


})

export default styles
