import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ScrollView, Dimensions,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import Slider from '@react-native-community/slider';

import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

import {withTranslation} from "react-i18next";


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get, toArray} from 'lodash'
import {connect} from "react-redux";
import FilterIcon from 'assets/image/SVG/Filter'

// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import LinearGradient from "react-native-linear-gradient";
import BackButton from "../../../assets/image/SVG/BackButton";
import Translate from 'services/translateTitle'


// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

class Filter extends Component {
    state = {
        search: '',
        isSpinnerActive: false,
        year: '',
        item1: [
            {label: 'car 1', value: 'uk'},
            {label: 'car 2', value: 'france'},
            {label: 'car 3', value: 'france'},
            {label: 'car 4', value: 'france'},
        ],
        text: undefined,
        tonna: undefined,
        carType: undefined,
        address: undefined,
        page: 1,
        isFetched: true,
        refreshing: false,
        scrollItem: false,
        value: 0,
        valueT: 0


        // queryParams:{}
    };

    componentDidMount() {
        this.PickerData()
        // this.Filter()
    }

    PickerData = (id) => {
        // this.setState({search});

        Routines.auth.getPickerData({
            request: {},
        }, this.props.dispatch)
            .then((data) => {
                    // this.getItems(1)
                    // NavigationService.goBack()
                },
            )
            .catch(e => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
            })
    };

    Filter(page = 1, isFetched = false, refreshing = true) {
        this.setState({isSpinnerActive: true})

        const {carType, address, text, tonna,value,valueT} = this.state
        const transportType = carType
        const location = address
        const baggageVolume = value!==0?value:undefined
        const baggageMass = valueT!==0?valueT:undefined

        // if(value!==0){
        //    baggageVolume = value
        // } else baggageVolume = undefined
        // if(valueT!==0){
        //     baggageMass = valueT
        // }

        this.setState({isFetched, refreshing})

        Routines.auth.filter({
            request: {
                queryParams: {
                    transportType: transportType || undefined,
                    location: location || undefined,
                    "baggageVolume[lte]": baggageVolume || undefined,
                    "baggageMass[lte]": baggageMass || undefined,
                    page: page,
                }
            },
        }, this.props.dispatch)
            .then((data) => {
                    this.setState({isSpinnerActive: false})
                    this.setState({scrollItem: true})
                    this.setState({isFetched: true, refreshing: false, page: page + 1})

                    // this.getItems(1)
                    // NavigationService.goBack()
                },
            )
            .catch(e => {
                this.setState({isFetched: true, refreshing: false})
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
            })
    }

    objectToArray = (object) => {
        const array = toArray(object)
        const obj = [{
            label: '',
            value: ''
        }]
        array.map((item, index) => {
            // console.log("b", item)
            obj[index] = {
                label: item,
                value: item
            }

        })

        console.log("OBJECT:", obj)
        return obj
    }
    Selected = (value, id) => {
        if (id === 1) {
            this.setState({carType: value})
        } else {
            console.log("dsds:", value)
            this.setState({address: value})
        }

        console.log("VALUE_S:", id)
    }

    data = () => {
        const filter = get(this.props.filter, 'data', [])
        return filter
    }

    render() {
        let {item1} = this.state
        let {t, getPickerData, currentLangCode, filter} = this.props
        let count = get(this.props, 'filter.pagination.count', [])
        console.log('filterdsds:', get(this.props, 'filter.pagination.count', []))
        const {refreshing, isFetched, page, numColumns} = this.state
        console.log("value:",this.state.value)
        // console.log("ToArray:",toArray(getPickerData.avtoType_uz))
        return (
            <Components.Layout>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>
                <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                style={styles.wrapper}>
                    <View style={styles.container}>
                        <View style={styles.row1}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton} color={colors.white}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('transport')}
                            </Text>
                            <TouchableOpacity style={styles.filter} onPress={() => {
                                this.setState({scrollItem: false, text: '', tonna: '',value: 0,valueT: 0})

                            }}>
                                <FilterIcon/>
                            </TouchableOpacity>
                        </View>

                        {
                            this.state.scrollItem ? (
                                <Components.InfiniteFlatList
                                    contentContainerStyle={styles.infinite}
                                    numColumns={1}
                                    // horizontal ={false}
                                    // columnWrapperStyle={styles.view}
                                    data={this.data()}
                                    renderItem={({item, index}) => {
                                        return (
                                            <Components.Item
                                                key={item.id}
                                                index={index}
                                                item={item}
                                                touch={() => {
                                                    NavigationService.navigate('singleItem', {
                                                        item: item
                                                    })
                                                }}
                                            />
                                        )
                                    }}
                                    loading={!isFetched}
                                    refreshing={refreshing}
                                    onRefresh={() => {
                                        this.Filter(1, true, false)
                                    }}
                                    onEndReached={() => {
                                        if (count >= page) {
                                            console.log("PAGE:", page)
                                            this.Filter(page, false, true)
                                        }
                                    }}
                                    ListHeaderComponent={({item, index}) => {
                                        return null
                                    }}
                                    emptyText="No Items"
                                    keyExtractor={(item, index) =>
                                        index
                                    }
                                    // onEndThreshold={0}
                                />
                            ) : (
                                <KeyboardAwareScrollView contentContainerStyle={styles.scroll}>
                                    <Components.ButtonPicker
                                        zIndex={5000}
                                        items={Translate.title(currentLangCode, getPickerData.avtoType_ru, getPickerData.avtoType_uz)}
                                        placeholder={t('type_auto')}
                                        select={(value) => this.Selected(value, 1)}

                                    />
                                    <Components.ButtonPicker
                                        zIndex={3000}
                                        items={Translate.title(currentLangCode, this.objectToArray(getPickerData.location_ru), this.objectToArray(getPickerData.location_uz))}
                                        placeholder={t('address')}
                                        select={(value) => this.Selected(value, 2)}

                                    />
                                    {/*<TextInput*/}
                                    {/*    // name={'name'}*/}
                                    {/*    // label={'name'}*/}
                                    {/*    // title={'Ваше Ф.И.О *'}*/}
                                    {/*    keyboardType={'number-pad'}*/}
                                    {/*    style={styles.TextInput}*/}
                                    {/*    placeholder={'100 m3'}*/}
                                    {/*    placeholderTextColor={colors.lightBlack}*/}
                                    {/*    value={this.state.text}*/}
                                    {/*    onChangeText={(text) => {*/}
                                    {/*        this.setState({text: text})*/}
                                    {/*    }}*/}
                                    {/*/>*/}
                                    {/*<TextInput*/}
                                    {/*    // name={'name'}*/}
                                    {/*    // label={'name'}*/}
                                    {/*    // title={'Ваше Ф.И.О *'}*/}
                                    {/*    keyboardType={'number-pad'}*/}
                                    {/*    style={styles.TextInput}*/}
                                    {/*    placeholder={'1 tonna'}*/}
                                    {/*    placeholderTextColor={colors.lightBlack}*/}
                                    {/*    value={this.state.tonna}*/}
                                    {/*    onChangeText={(text) => {*/}
                                    {/*        this.setState({tonna: text})*/}
                                    {/*    }}*/}
                                    {/*/>*/}
                                    <Text style={styles.mass}>
                                        {this.state.value} м3
                                    </Text>
                                    <Slider
                                        style={{width: '100%', height: 60}}
                                        minimumValue={0}
                                        maximumValue={200}
                                        step={1}
                                        // inverted={true}
                                        thumbTintColor={colors.white}
                                        minimumTrackTintColor={colors.brandGreen}
                                        maximumTrackTintColor="#fff"
                                        onValueChange={value => this.setState({value: value})}
                                    />
                                    <Text style={styles.mass}>
                                        {this.state.valueT} тонна
                                    </Text>
                                    <Slider
                                        style={{width: '100%', height: 60}}
                                        minimumValue={0}
                                        maximumValue={100}
                                        thumbTintColor={colors.white}
                                        step={1}
                                        minimumTrackTintColor={colors.brandGreen}
                                        maximumTrackTintColor="#fff"
                                        onValueChange={value => this.setState({valueT: value})}
                                    />
                                    <Components.Button text={t('done')}
                                                       color={colors.darkBlue}
                                                       background={colors.button}
                                                       padding={Height * 0.13}
                                                       isActive={this.state.isSpinnerActive}
                                                       touch={() => {
                                                           this.Filter()
                                                       }}
                                    />
                                </KeyboardAwareScrollView>
                            )
                        }

                    </View>
                </LinearGradient>
            </Components.Layout>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        getPickerData: state.profile.getPickerData,
        currentLangCode: state.language.lang,
        filter: state.profile.filter
    };
};
Filter = connect(mapStateToProps)(Filter)
export default withTranslation('main')(Filter)
