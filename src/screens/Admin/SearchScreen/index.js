import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ScrollView,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import {SearchBar} from 'react-native-elements';
import Filter from 'assets/image/SVG/Filter'


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import Icon from 'react-native-vector-icons/SimpleLineIcons'
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import LinearGradient from "react-native-linear-gradient";
import InfiniteFlatList from "../../../components/FlatListInfinity";
import {saveUser} from "../../../services/actions";
import {DrawerContentScrollView} from "@react-navigation/drawer";
import {SET_LANGUAGE} from "../../../services/constants";

// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

class HomeScreen extends Component {
    state = {
        search: '',
        isSpinnerActive: false,
        year: '',
        isFetched: true,
        refreshing: false,
        page: 1,
        isActive: false,
        searchArray: '',
        item1: [
            {label: 'RU', value: 'ru'},
            {label: 'UZ', value: 'uz'},

        ],
        // numColumns: 2,
    };

    componentDidMount() {
        this.getItems(1)
    }

    onLangClicked = (lang) => {
        // console.log("LANG:",lang)
        let {i18n, dispatch} = this.props;
        i18n.changeLanguage(lang, () => {
            dispatch({type: SET_LANGUAGE, lang})
        })
    }

    getItems(page = 1, isFetched = false, refreshing = true) {

        this.setState({isFetched, refreshing})
        Routines.auth.getItems({
            request: {
                page: page
            }
        }, this.props.dispatch)
            .then((data) => {
                    // let items=get(this.props.items, 'data', [])
                    //     this.renderItems(items)
                    this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch((e) => {
                this.setState({isFetched: true, refreshing: false})
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
            })
    }

    updateSearch = (search) => {
        this.setState({search});

        Routines.auth.search({
            request: {
                search: search
            },
        }, this.props.dispatch)
            .then((data) => {
                    this.setState({searchArray: search});


                    // let items=get(this.props, 'search', [])
                    // this.renderItems(items)
                    // year = this.setState.data.year
                    // data === "success"?NavigationService.reset('panel', 0): ToastAndroid.show("Siz muvaffaqiyatli ro'yhatdan o'tdingiz !", ToastAndroid.SHORT);

                },
            )
            .catch(e => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
            })
    };
    data = () => {
        return get(this.props.items, 'data', [])
    }

    render() {
        let {t, items, search, filter, currentLangCode} = this.props
        const {refreshing, isFetched, page, numColumns, item1} = this.state
        console.log("ITEMS:", items)
        console.log("Search:", get(this.props, 'search', []))
        console.log("Filter:", filter)
        let count = get(this.props, 'items.pagination.count', [])
        console.log("COUNT:", count)
        // let {item1} = this.state
        return (
            <Components.Layout>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>
                <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                style={styles.wrapper}>
                    <View style={styles.container}>
                        <View style={styles.row}>
                            <TouchableOpacity style={styles.logout} onPress={() => {
                                this.props.dispatch(saveUser({}));
                            }}>
                                <Icon name={'logout'} color={colors.white} size={22}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('transport')}
                            </Text>
                            <View style={styles.row2}>
                                <Components.SelectLang
                                    zIndex={5000}
                                    items={item1}
                                    // style={{}}
                                    placeholder={currentLangCode === 'uz' ? 'UZ' : 'RU'}
                                    select={(value) => this.onLangClicked(value)}
                                    currentLangCode={currentLangCode}

                                />
                                <TouchableOpacity style={styles.filter} onPress={() => {
                                    NavigationService.navigate('filter')
                                }}>
                                    <Filter/>
                                </TouchableOpacity>

                            </View>


                        </View>
                        <SearchBar
                            placeholder={t('search')}
                            onChangeText={(value) => this.updateSearch(value)}
                            value={this.state.search}
                            lightTheme={true}
                            inputStyle={{color: '#000',}}
                            containerStyle={styles.searchContainer}
                            inputContainerStyle={styles.input}

                        />
                        {
                            this.state.searchArray.length ? (
                                <ScrollView contentContainerStyle={styles.scroll}>
                                    {get(this.props, 'search', []).map((item, index) => {
                                        return (<Components.Item
                                            key={index}
                                            index={index}
                                            item={item}
                                            touch={() => {
                                                NavigationService.navigate('singleItem', {
                                                    item: item
                                                })
                                            }}
                                        />)
                                    })}
                                </ScrollView>

                            ) : (
                                <Components.InfiniteFlatList
                                    contentContainerStyle={styles.scroll}
                                    numColumns={1}
                                    // horizontal ={false}
                                    // columnWrapperStyle={styles.view}
                                    data={this.data()}
                                    renderItem={({item, index}) => {
                                        return (
                                            <Components.Item
                                                key={item.id}
                                                index={index}
                                                item={item}
                                                touch={() => {
                                                    NavigationService.navigate('singleItem', {
                                                        item: item
                                                    })
                                                }}
                                            />
                                        )
                                    }}
                                    loading={!isFetched}
                                    refreshing={refreshing}
                                    onRefresh={() => {
                                        this.getItems(1, true, false)
                                    }}
                                    onEndReached={() => {
                                        if (count >= page) {
                                            console.log("PAGE:", page)
                                            this.getItems(page, false, true)
                                        }
                                    }}
                                    ListHeaderComponent={({item, index}) => {
                                        return null
                                    }}
                                    emptyText="No Items"
                                    keyExtractor={(item, index) =>
                                        index
                                    }
                                    // onEndThreshold={0}
                                />
                            )
                        }

                    </View>
                    {/*<StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>*/}
                </LinearGradient>
            </Components.Layout>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        items: state.profile.items,
        search: state.profile.search,
        filter: state.profile.filter,
        currentLangCode: state.language.lang,
    };
};
HomeScreen = connect(mapStateToProps)(HomeScreen)
export default withTranslation('main')(HomeScreen)
