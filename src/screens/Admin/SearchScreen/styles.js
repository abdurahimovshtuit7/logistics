import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {statusBarHeigth} from 'assets/styles/commons'


const styles = StyleSheet.create({
    container:{
        flex: 1,
        width:'100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor:colors.darkBlue,
        paddingTop:statusBarHeigth,
        // borderWidth: 1,


    },
    row:{
        flexDirection:'row',
        paddingHorizontal:10,
        // zIndex:1,
        justifyContent: 'space-between',
        // alignItems: 'center',
    },
    row2:{
        flexDirection:'row',
        paddingHorizontal:10,
        // zIndex:5,
        justifyContent: 'space-between',
        // alignItems: 'center',
    },

    scroll:{
        paddingVertical:8,
        paddingBottom:20,
    },
    searchContainer:{
        // marginTop:50,
        // marginLeft:37,
        // marginRight:20,
        marginHorizontal:20,
        marginVertical: 10,
        padding:0,

        backgroundColor:'transparent',
        // borderWidth:1,
        borderRadius:5,
        borderColor:'transparent'
    },
    input:{
        backgroundColor:'#fff',
        // borderWidth: 1,
        padding:0,
        // marginVertical:5
    },
    text:{
        textAlign:'center',
        fontSize:24,

    },
    title:{
      color:colors.white,
        justifyContent:'center',
        textAlign: 'center',
        fontFamily:'Rubik-Medium',
        fontSize: 18,
        paddingVertical: 10,
        width: '45%',
        // borderWidth: 1,

    },
    filter:{
        // borderWidth:1,
        paddingLeft: 10,
        paddingRight:4,
        paddingVertical:15
    },
    logout:{
        padding:10,
        // borderWidth:1
    },
    wrapper: {
        height: "100%",
        width:'100%',
        alignItems: 'center',
    },

})

export default styles
