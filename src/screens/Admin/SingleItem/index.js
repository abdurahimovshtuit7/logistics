import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ScrollView,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import {SearchBar} from 'react-native-elements';
import Filter from 'assets/image/SVG/Filter'



import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import LinearGradient from "react-native-linear-gradient";
import BackButton from "../../../assets/image/SVG/BackButton";
// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

class HomeScreen extends Component {
    state = {
        search: '',
        isSpinnerActive: false,
        year: ''
    };

    Delete = (id) => {
        this.setState({isSpinnerActive:true});
        Routines.auth.Delete({
            request: {
                id: id
            },
        }, this.props.dispatch)
            .then((data) => {
                this.getItems(1)
                this.setState({isSpinnerActive:false});
                NavigationService.goBack()
                },
            )
            .catch(e => {
                console.log('Error:',e)
                let status = get(e,'message.error','Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                this.setState({isSpinnerActive:false});


            })
    };
    getItems(page = 1, isFetched = false, refreshing = true) {

        this.setState({isFetched, refreshing})
        Routines.auth.getItems({
            request: {
                page: page
            }
        }, this.props.dispatch)
            .then((data) => {
                    // let items=get(this.props.items, 'data', [])
                    //     this.renderItems(items)
                    // this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch((e) => {
                console.log('Error:',e)
                let status = get(e,'message.error','Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
            })
    }
    render() {
        let {t,route}= this.props
        console.log("PARAMS:",get(route.params,'item',{}))
        let data = get(route.params,'item',{})
        return (
            <Components.Layout>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>
                <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                style={styles.wrapper}>
                    <View style={styles.container}>
                        <View style={styles.row1}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton} color={colors.white}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('transport')}
                            </Text>
                        </View>

                        <ScrollView contentContainerStyle={styles.scroll}>
                            <Components.Description
                                data = {data}
                            />
                            <Components.ImageViewer doc={t('document')}
                                                    tex={t('tex_pass')}
                                                    pass={t('pass')}
                                                    data={data}
                            />
                            <Components.Button text={t('Delete')}
                                               color={colors.white}
                                               background={colors.red}
                                               padding = {'20%'}
                                               paddingHorizantal = {20}
                                               isActive={this.state.isSpinnerActive}
                                               touch={() => {
                                                  const id = get(data,'_id','')
                                                  this.Delete(id)
                                               }}
                            />

                        </ScrollView>
                    </View>
                    {/*<StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>*/}
                </LinearGradient>
            </Components.Layout>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {

    };
};
HomeScreen = connect(mapStateToProps)(HomeScreen)
export default withTranslation('main')(HomeScreen)
