import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {statusBarHeigth} from 'assets/styles/commons'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor:colors.darkBlue,
        paddingTop:statusBarHeigth,
    },

    scroll:{
        paddingVertical:8,
        paddingBottom:30,

        // paddingHorizontal: 10,
    },
    text:{
        textAlign:'center',
        fontSize:24,

    },
    title:{
        color:colors.white,
        justifyContent:'center',
        textAlign: 'center',
        fontFamily:'Rubik-Medium',
        fontSize: 18,
        // paddingVertical: 10,
        width: '80%'

    },
    wrapper: {
        height: "100%",
        width:'100%',
        alignItems: 'center',
    },
    backButton:{
        paddingLeft:20,
    },
    touchable:{
        paddingHorizontal:10,
    },
    row1:{
        // borderWidth:1,
        // paddingBottom: '8%',
        paddingLeft:10,
        // paddingVertical:10,
        paddingBottom: 10,
        // paddingTop:50,
        width:'100%',
        flexDirection:'row',
        backgroundColor:'transparent',
        alignItems:'center'
    },

})

export default styles
