import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ImageBackground, Keyboard,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {Trans, withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";
import moment from 'moment';
import 'moment-duration-format';

// -----SVG----
import Track from 'assets/image/SVG/Track'
import BackButton from 'assets/image/SVG/BackButton'
import {HeaderBackButton} from "@react-navigation/stack";
import LinearGradient from "react-native-linear-gradient";

class Login extends Component {
    componentDidMount() {
        this.timeHandler()
    }

    state = {
        code: ['', '', '', ''],
        time: 3,
        isSpinnerActive: false,
        oneSignalId: ''
    };

    timeHandler = () => {
        clearInterval(this.state.timer);
        this.setState({time: 3, timer: null});

        let timer = setInterval(() => {
            this.setState(state => ({time: state.time - 1}));
            if (this.state.time === 0) {
                clearInterval(timer);
                this.setState({timer: null})
            }
        }, 1000);

        this.setState({timer}, () => {
            // this.smsListener()
        });
    };


    approvePhone(code) {
        console.log('CODE:::', code)
        Keyboard.dismiss();
        let {route} = this.props;
        let {phone} = get(route, `params`, {});
        this.setState({isSpinnerActive: true});
        // Routines.user.approvePhone({request:{
        //         data: { code, phone },
        //         phone
        //     }}, this.props.dispatch)
        //     .then((e) => {
        //             let userData = get(e, 'response.data.success.user', { status: 0 });
        //             console.log("e", userData);
        //             this.setOneSignalID();
        //             let tabName = 'tabNavigator';
        //             if(userData.status === 0){
        //                 tabName = "updateProfile"
        //             }
        //             this.setState({isSpinnerActive: false});
        //             NavigationService.reset(tabName, 0)
        //         },
        //         (e)=>{
        //             console.log("e", e)
        //             let status = get(e, `message.message`, 'something went wrong');
        //             this.setState({isSpinnerActive: false});
        //             if(e.message === "NETWORK_ERROR"){
        //                 Components.Toast.show()
        //             }
        //             else {
        //                 Components.Toast.show(status)
        //             }
        //         })
    }

    render() {
        // const {handleSubmit,errors} = this.props
        let {route, t,userData} = this.props;
        let {phone, getApproveCode} = get(route, 'params', {});
        let {code, time, isSpinnerActive} = this.state;
        let timer = moment.duration(time, 'seconds').format('mm:ss', {trim: false});
        console.log("USER_DATA:",userData)
        return (
            <Components.Layout>
                <StatusBar backgroundColor={colors.statusbar} barStyle="dark-content" translucent={false}/>
                <ImageBackground source={require('assets/image/tracker.png')}
                                 style={styles.brand}
                                 resizeMode={'cover'}
                                 blurRadius={1}
                    // resizeMethod={'resize'}
                >
                    <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                    style={styles.wrapper}>
                        <View style={styles.row1}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('reg')}
                            </Text>
                        </View>
                        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled"
                                                 enableOnAndroid={false}
                                                 enableResetScrollToCoords={false}
                                                 contentContainerStyle={styles.wrappers}>

                            <Components.Layout style={styles.view}>
                                <Text style={styles.mainTitle}>
                                    {t('kod')}
                                </Text>
                                <View style={styles.inputWrap}>
                                    {
                                        code && code.map((value, index) => {
                                            let isFocused = this.refs[index] && this.refs[index].isFocused();
                                            return (
                                                <TextInput
                                                    ref={(input) => {
                                                        this.refs = {...this.refs, [index]: input};
                                                    }}
                                                    value={value}
                                                    key={index}
                                                    style={[
                                                        styles.input,
                                                        isFocused ? styles.activeInput : {},
                                                        code[index] && !isFocused ? styles.fulfilledField : {}
                                                    ]}
                                                    autoFocus={index === 0}
                                                    // placeholder={'*'}
                                                    keyboardType={"number-pad"}
                                                    returnKeyType={"next"}
                                                    textContentType={"oneTimeCode"}
                                                    onChangeText={(text) => {
                                                        console.log("text", text)
                                                        if (text.length === 4) {
                                                            this.setState({code: text.split('')}, () => {
                                                                this.approvePhone(text)
                                                            })
                                                        } else {
                                                            let Code = [...code];
                                                            if (text.length <= 1)
                                                                Code[index] = text;
                                                            else {
                                                                Code[index] = text.slice(-1)
                                                            }
                                                            if (text) {
                                                                if (this.refs[index + 1])
                                                                    this.refs[index + 1].focus();
                                                            }
                                                            this.setState({code: Code}, () => {
                                                                if (index === 3 && text)
                                                                    this.approvePhone(Code.join(''))
                                                            })
                                                        }
                                                    }}
                                                    onKeyPress={({nativeEvent}) => {
                                                        if (nativeEvent.key === 'Backspace') {
                                                            if (!code[index] && this.refs[index - 1]) {
                                                                this.refs[index - 1].focus();
                                                            }
                                                        }
                                                    }}
                                                />
                                            )
                                        })
                                    }
                                </View>


                                <TouchableOpacity
                                    disabled={time > 0}
                                    onPress={() => {
                                        if (getApproveCode) {
                                            this.timeHandler();
                                            getApproveCode(phone)
                                        }
                                    }}
                                >
                                    {time ? (<Text style={styles.titleCode}>
                                        {t('time_limit')} <Text style={styles.time}>{timer}</Text>
                                    </Text>) : (<Components.DownloadButton text={t('qayta_yuborish')}
                                                                           color={colors.text}
                                                                           margin={20}
                                                                           background={colors.white}
                                                                           border={1}
                                                                           icon={false}
                                                                           touch={() => {
                                                                               // this.setState({time:3})
                                                                               this.timeHandler()
                                                                           }}
                                    />)}
                                </TouchableOpacity>


                                <Components.Button text={t('continue')}
                                                   color={colors.white}
                                                   background={colors.green}
                                                   touch={() => {
                                                       NavigationService.navigate('password')
                                                   }}
                                />
                                {/*<Text style={styles.footer}>*/}
                                {/*    Spacemos DT*/}
                                {/*</Text>*/}
                            </Components.Layout>
                        </KeyboardAwareScrollView>
                    </LinearGradient>
                </ImageBackground>
            </Components.Layout>
    )
    }
    }

    Login = withFormik({
        mapPropsToValues: () => ({
        phone: "",
    }),
        validationSchema: ({t}) => {
        return Yup.object().shape({
        phone: Yup.string()
        .required(t("required"))
        .min(18, 'Belgilar soni kam!'),
    });
    },
        handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);
        NavigationService.navigate('singUp')


        let {name, phone,email,password,confirmPassword} = values;
        // let {route} = this.props
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)
        // setSubmitting(true);
        // Routines.auth.signUp({
        //     request: {
        //         data: {
        //             name: name,
        //             phone:phone,
        //             email:email,
        //             type:'personal',
        //             password: password,
        //             confirmPassword:confirmPassword
        //         }
        //     }
        // }, props.dispatch)
        //     .then((data) => {
        //         if(get(data.response,'data.success',false)){
        //             NavigationService.navigate('home')
        //             return (
        //                 Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
        //             )
        //
        //         } else {
        //             console.log("Success")
        //         }
        //     })
        //     .catch(e => {
        //         // setSubmitting(false);
        //         // if (e.message === "NETWORK_ERROR") {
        //         // }
        //     });
    }
    })(Login);
    const mapStateToProps = (state, ownProps) => {
        return {
            userData:state.user.user
        };
    };
    Login = connect(mapStateToProps)(Login)
    export default withTranslation('main')(Login)
