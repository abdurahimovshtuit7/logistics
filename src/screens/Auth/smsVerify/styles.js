import {Dimensions, StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {horizontal,statusBarHeigth} from "assets/styles/commons";
// import {statusBarHeigth} from 'assets/styles/commons'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        alignItems: 'center',
        backgroundColor:colors.white,
        // justifyContent: 'center',
        // backgroundColor:'grey',

    },
    logo:{
        marginTop:"30%",
    },
    wrapper: {
        height: "100%",
        width:'100%',
        // alignItems: 'center',
    },
    brand:{
        // paddingTop:"30%",

        // backgroundColor:'grey',
        // width:'100%',
        // height:'100%',
        flex: 1,
        backgroundColor: colors.white,
        // width: '100%',
        // paddingHorizontal:-10,
        // overflow: 'hidden',
        // blurRadius:6
        // borderWidth:1,

    },
    text:{
        textAlign:'center',
        fontSize:18,
        color:colors.white,
        paddingTop:17,
        width:110,
        fontFamily:'Rubik-Bold',
        // borderWidth:1
    },
    text1:{
        color:colors.white,
        fontSize: 35,
        width:224,
        textAlign:'center',
        // fontWeight:'bold',
        fontFamily:'Rubik-Medium',
        paddingTop: '25%',
        // backgroundColor: 'transparent'
        // borderWidth:1,
    },
    backButton:{
        // alignItems:'center'
        paddingLeft:20,
    },
    view:{
        // flex:1,
        // // marginTop:-Height/2.47,
        // paddingTop:40,
        // paddingHorizontal:30,
        // zIndex:1,
        // width:'100%',
        // // height:205,
        // backgroundColor: colors.white,
        // borderTopRightRadius:25,
        // borderTopLeftRadius:25,
        flex: 1,
        paddingTop: 40,
        paddingHorizontal: 30,
        paddingBottom:40,
        marginBottom:0,
        marginTop:'90%',
        backgroundColor: colors.white,
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        width:'100%',
        zIndex: 1,
        // borderWidth:1,
    },
    footer:{
        textAlign:'center',
        // paddingTop:0.08*Height,
        position:'absolute',
        bottom:20,
        left:'44%',
        fontFamily:'Rubik-Regular',
        color:colors.footerColor
    },
    row:{
        // borderWidth:1,
        paddingBottom: 10,
        paddingLeft: 10,
        marginTop:statusBarHeigth,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'transparent',
        alignItems: 'center'
        // paddingLeft:10,
        // paddingVertical:20,
        // width:'100%',
        // flexDirection:'row',
        // backgroundColor:'transparent',
        // alignItems:'center'
    },
    title:{
        width:'85%',
        color:colors.white,
        fontFamily:'Rubik-Medium',
        fontSize:18,
        // borderWidth:1,
        textAlign:'center',
    },
    touchable:{
        // borderWidth:1,
        paddingHorizontal:10,

    },
    wrappers: {
        // flex:1,
        flexGrow:1,

        // paddingVertical: 16
    },
    error:{
        color:'#e00a13',
        fontSize: 12,
        paddingBottom:15,
        // marginHorizontal:'9%'
    },
    row1:{
        // borderWidth:1,
        // paddingBottom:'100%',
        // paddingLeft:10,
        // // paddingVertical:10,
        // marginTop:statusBarHeigth,
        // width:'100%',
        // flexDirection:'row',
        // backgroundColor:'transparent',
        // alignItems:'center',
        paddingBottom: 10,
        paddingLeft: 10,
        marginTop:statusBarHeigth,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'transparent',
        alignItems: 'center'
    },

    inputWrap: {
        ...horizontal,
        justifyContent: 'center',
        // marginVertical: 22
    },
    input: {
        marginRight: 10,
        // borderRadius: 10,
        width: 40,
        height: 40,
        textAlign: 'center',
        // borderWidth: 1,
        borderColor: '#D6D9DD',
        color: '#344355',
        borderBottomWidth:1,
        // fontFamily: 'SFProDisplay-Medium',
        fontSize: 16,
        // backgroundColor: '#f3f3f3'
    },
    activeInput: {
        borderBottomWidth: 1,
        borderColor: colors.green,
    },
    fulfilledField: {
        borderColor: colors.green,
        // backgroundColor: '#FAFBFB',

        //iOS
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0,
        shadowRadius: 0,
        //Android
        elevation: 0
    },
    time: {
        // marginTop: 38,
        // marginBottom: 33,
        fontSize: 15,
        // textDecorationLine: 'underline',
        fontFamily:'Rubik-Bold',
        color: colors.font,
        alignSelf: "center"
    },
    titleCode: {
        fontFamily:'Rubik-Regular',

        fontSize: 14,
        textAlign:'center',
        paddingTop:10,
        paddingBottom: 20,
        color: colors.font
    },
    mainTitle:{
        fontSize:14,
        fontFamily:'Rubik-Medium',
        color:colors.black,
        paddingBottom:11,
        // paddingBottom:8
    },


})

export default styles
