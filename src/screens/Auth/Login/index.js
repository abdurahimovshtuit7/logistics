import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ImageBackground,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import * as Animatable from 'react-native-animatable';


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";

// -----SVG----
import BackButton from 'assets/image/SVG/BackButton'
import LinearGradient from "react-native-linear-gradient";

class Login extends Component {
    render() {
        const {handleSubmit, errors,t} = this.props
        return (
            // <SafeAreaView style={styles.container}>
            <Components.Layout>
                {/*<StatusBar backgroundColor={colors.statusbar} barStyle="dark-content" translucent={false}/>*/}

                <ImageBackground
                    // source={require('assets/image/tracker.png')}
                                 style={styles.brand}
                                 // resizeMode={'cover'}
                                 blurRadius={1}
                >
                    <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                    style={styles.wrapper}>
                        <View style={styles.row}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton} color={colors.white}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('reg')}
                            </Text>
                        </View>
                        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled"
                                                 enableOnAndroid={false}
                                                 enableResetScrollToCoords={false}
                                                 contentContainerStyle={styles.wrappers}>
                            <Animatable.View style={styles.view}
                                             animation = "fadeInUpBig">

                                <Field
                                    name={'phone'}
                                    label={'phone'}
                                    title={t('tel_nomer')}
                                    placeHolder={'(+998) 99 999 99 99'}
                                    component={Components.InputText}
                                    mask={"(+998) [00] [000] [00] [00]"}
                                    keyboards={'number-pad'}
                                />
                                {errors.phone ? (
                                    <Text style={styles.error}>{errors.phone}</Text>
                                ) : null}
                                <Text style={styles.text2}>{t("description")}</Text>

                                <Components.Button text={t('continue')}
                                                   color={colors.white}
                                                   background={colors.green}
                                                   touch={() => {
                                                       handleSubmit()
                                                   }}
                                />
                                {/*<Text style={styles.footer}>*/}
                                {/*    Spacemos DT*/}
                                {/*</Text>*/}
                            </Animatable.View>
                        </KeyboardAwareScrollView>

                    </LinearGradient>
                </ImageBackground>

            </Components.Layout>
            // {/*</SafeAreaView>*/}
        )
    }
}

Login = withFormik({
    mapPropsToValues: () => ({
        phone: "",
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            phone: Yup.string()
                .required(t("required"))
                .min(18, 'Belgilar soni kam!'),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);
        NavigationService.navigate('password',{phone:values.phone})


        let {name, phone, email, password, confirmPassword} = values;
        // let {route} = this.props
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)
        // setSubmitting(true);
        // Routines.auth.signUp({
        //     request: {
        //         data: {
        //             name: name,
        //             phone:phone,
        //             email:email,
        //             type:'personal',
        //             password: password,
        //             confirmPassword:confirmPassword
        //         }
        //     }
        // }, props.dispatch)
        //     .then((data) => {
        //         if(get(data.response,'data.success',false)){
        //             NavigationService.navigate('home')
        //             return (
        //                 Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
        //             )
        //
        //         } else {
        //             console.log("Success")
        //         }
        //     })
        //     .catch(e => {
        //         // setSubmitting(false);
        //         // if (e.message === "NETWORK_ERROR") {
        //         // }
        //     });
    }
})(Login);
const mapStateToProps = (state, ownProps) => {
    return {};
};
Login = connect(mapStateToProps)(Login)
export default withTranslation('main')(Login)
