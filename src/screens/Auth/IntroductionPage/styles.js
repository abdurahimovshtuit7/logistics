import {Dimensions, StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    logo: {
        marginTop: Height*0.14,
        // paddingBottom: 70,
    },
    wrapper: {
        height: "100%",
        backgroundColor: colors.darkBlue,
        width: '100%',
    },
    brand: {
        // paddingTop:"30%",
        // alignItems: 'center',
        // alignItems: 'center',
        height: '100%',
        flex: 1,
        backgroundColor: colors.darkBlue,
        width: '100%',
        // paddingHorizontal: -10,
        // overflow: 'hidden',
        // blurRadius:6
        // borderWidth:1,

    },
    text: {
        textAlign: 'center',
        fontSize: 37,
        color: colors.white,
        // alignItems: 'center',
        paddingTop: 17,
        width: '80%',

        fontFamily: 'Rubik-Bold',
        // borderWidth:1
    },
    text1: {
        color: colors.white,
        fontSize: 33,
        width: 224,
        textAlign: 'center',
        // fontWeight:'bold',
        fontFamily: 'Rubik-Medium',
        paddingTop: Height*0.09,
        // marginBottom: 80,
        // backgroundColor: 'transparent'
        // borderWidth:1,
    },
    view: {
        flex: 1,
        flexGrow: 1,
        // justifyContent:'flex-end',
        // marginTop:-Height/3.2,
        backgroundColor: colors.white,
        paddingVertical:40,

        paddingHorizontal: 30,
        width: '100%',
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        // zIndex: 1,
        // paddingTop: '7%',

    },
    footer: {
        textAlign: 'center',
        paddingTop: Height*0.08,
        paddingBottom: 20,
        // justifyContent:'flex-end',
        fontFamily: 'Rubik-Regular',
        color: colors.footerColor
    },
    wrappers: {
        marginTop: 20,
        flex:1,
        // height:'100%',
        // backgroundColor:colors.white,
        // paddingBottom: 20,
        width: '100%',
    },
    center: {
        alignItems: 'center',
        // marginTop:90,
    }


})

export default styles
