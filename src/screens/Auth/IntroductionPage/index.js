import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View,
    TextInput,
    ImageBackground,
    ScrollView, Dimensions,
    // Animatable

} from "react-native";
import * as Animatable from 'react-native-animatable';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import Logo from 'assets/image/SVG/Logo'

const Height = Dimensions.get('window').height;

// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

// -----SVG----
import Track from 'assets/image/SVG/Track'
import LinearGradient from "react-native-linear-gradient";

class IntroductionPage extends Component {
    handleTextRef = ref => this.text = ref;

    render() {
        const {t} = this.props
        return (
            // <SafeAreaView style={styles.container}>
            <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                            style={styles.wrapper}
            >
                <Components.Layout>
                    <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>
                    <ScrollView
                        // source={require('assets/image/tracker.png')}
                        contentContainerStyle={styles.brand}
                        // resizeMode={'cover'}
                        // blurRadius={1}
                        // resizeMethod={'resize'}
                    >

                        <View style={styles.center}>
                            <Animatable.View animation='bounceInLeft'
                                             duration={3000}
                            >
                                <Logo style={styles.logo} size={Height * 0.12}/>
                            </Animatable.View>


                            <Animatable.Text style={styles.text}
                                // ref={this.handleTextRef}
                                             animation='bounceInLeft'
                                             duration={3000}
                            >
                                Logen Berg
                            </Animatable.Text>
                            <Text style={styles.text1}>
                                {t('xush_kelibsiz')}
                            </Text>
                        </View>

                        <View style={styles.wrappers}>

                            <Animatable.View style={styles.view}
                                             animation="fadeInUpBig"
                            >

                                <Components.Button text={t('kirish')}
                                                   color={colors.white}
                                                   background={colors.green}
                                                   touch={() => {
                                                       // handleSubmit()
                                                       // this.text.transitionTo({ opacity: 0.2 })
                                                       NavigationService.navigate('signIn')
                                                   }}
                                />
                                <Components.Button text={t('reg')}
                                                   color={colors.green}
                                                   background={colors.white}
                                                   padding={15}
                                                   border={1}
                                                   touch={() => {
                                                       // handleSubmit()
                                                       NavigationService.navigate('login')
                                                   }}
                                />

                                <Components.Layout style={{flex:1,justifyContent:'flex-end'}}>
                                    <Text style={styles.footer}>
                                       Powered by Soft Plus
                                    </Text>
                                </Components.Layout>


                            </Animatable.View>
                        </View>


                    </ScrollView>
                </Components.Layout>
            </LinearGradient>
            // </SafeAreaView>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {};
};
IntroductionPage = connect(mapStateToProps)(IntroductionPage)
export default withTranslation('main')(IntroductionPage)
