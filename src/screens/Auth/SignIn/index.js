import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    Alert,
    StatusBar, View, TextInput, ImageBackground,
} from "react-native";
import * as Animatable from 'react-native-animatable';

import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";

// -----SVG----
import BackButton from 'assets/image/SVG/BackButton'
import LinearGradient from "react-native-linear-gradient";
import OneSignal from "react-native-onesignal";

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.onIds = this.onIds.bind(this);
        OneSignal.addEventListener('ids', this.onIds);
    }
    state = {
        icon1: 'eye-off',
        password1: true,
        isActive:false

    }
    componentWillUnmount() {
        OneSignal.removeEventListener('ids', this.onIds);
    }
    onIds(device) {
        console.log('Device info: ', device);
        console.log(device.userId);
        this.props.setFieldValue('oneID',device.userId)
    }
    _changeIcon1 () {
        this.setState(prevState => ({
            icon1: prevState.icon1 === 'eye' ? 'eye-off' : 'eye',
            password1: !prevState.password1,
        }))
    }

    render() {
        const {handleSubmit, errors,t} = this.props
        return (
            // <SafeAreaView style={styles.container}>
            <Components.Layout>
                {/*<StatusBar backgroundColor={colors.statusbar} barStyle="dark-content" translucent={false}/>*/}

                <ImageBackground
                    // source={require('assets/image/tracker.png')}
                                 style={styles.brand}
                                 // resizeMode={'cover'}
                                 blurRadius={1}
                >
                    <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                    style={styles.wrapper}>
                        <View style={styles.row}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton} color={colors.white}/>
                            </TouchableOpacity>

                            {/*<Text style={styles.title}>*/}
                            {/*    {t('reg')}*/}
                            {/*</Text>*/}
                        </View>
                        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled"
                                                 enableOnAndroid={false}
                                                 enableResetScrollToCoords={false}
                                                 contentContainerStyle={styles.wrappers}>
                            <Animatable.View style={styles.view}
                                             animation = "fadeInUpBig"
                            >

                                <Field
                                    name={'phone'}
                                    label={'phone'}
                                    title={t('tel_nomer')}
                                    component={Components.InputText}
                                    placeHolder={'(+998) 99 999 99 99'}
                                    mask={"(+998) [00] [000] [00] [00]"}
                                    keyboards={'number-pad'}
                                />
                                {errors.phone ? (
                                    <Text style={styles.error}>{errors.phone}</Text>
                                ) : null}
                                <Text style={styles.text2}>{t("description")}</Text>
                                <Field
                                    name={'password'}
                                    label={'password'}
                                    title={t('parol')}
                                    placeHolder={t('parol')}
                                    component={Components.SecureInput}
                                    secureTextEntry={this.state.password1}
                                    changeIcon={()=>this._changeIcon1()}
                                    iconName={this.state.icon1}
                                    // mask={"(+998) [00] [000] [00] [00]"}
                                />
                                {errors.password ? (
                                    <Text style={styles.error}>{errors.password}</Text>
                                ) : null}
                                <Components.Button text={t('continue')}
                                                   color={colors.white}
                                                   background={colors.green}
                                                   touch={() => {
                                                       handleSubmit()
                                                   }}
                                                   isActive={this.props.isSubmitting}
                                />
                                {/*<Text style={styles.footer}>*/}
                                {/*    Spacemos DT*/}
                                {/*</Text>*/}
                            </Animatable.View>
                        </KeyboardAwareScrollView>

                    </LinearGradient>
                </ImageBackground>

            </Components.Layout>
            // {/*</SafeAreaView>*/}
        )
    }
}

SignIn = withFormik({
    mapPropsToValues: () => ({
        phone: "",
        password:"",
        oneID:''
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            phone: Yup.string()
                .required(t("required")),
                // .min(18, 'Belgilar soni kam!'),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            password: Yup.string()
                // .min(8, 'Belgilar soni kam!')
                .max(16, 'Belgilar soni juda ko\'p!')
                .required(t("required")),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);
        // this.setState({isActive: true})

        let { phone, email, password, confirmPassword,oneID} = values;
        let onlyNums = phone.replace(/[^\d]/g, '');
        setSubmitting(true);
        console.log("SUBMIT:",setSubmitting)
        Routines.auth.Login({
            request: {
                data: {
                    password: password,
                    phonenumber: onlyNums,
                    oneID:oneID
                }
            }
        }, props.dispatch)
            .then((data) => {
                // this.setState({isActive: false})
                setSubmitting(false);

                if(get(data.response,'data.success',false)){
                    NavigationService.navigate('drawer'),
                        props.dispatch(saveUser(data.response.data))
                }
            })
            .catch(e => {
                console.log('Error:',e)
                let status = get(e,'message.error','Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                setSubmitting(false);
            });
    }
})(SignIn);
const mapStateToProps = (state, ownProps) => {
    return {};
};
SignIn = connect(mapStateToProps)(SignIn)
export default withTranslation('main')(SignIn)
