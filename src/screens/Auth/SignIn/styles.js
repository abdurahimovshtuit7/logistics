import {Dimensions, StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {statusBarHeigth} from 'assets/styles/commons'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white,
        // justifyContent: 'center',
        // backgroundColor:'grey',

    },
    logo: {
        marginTop: "30%",
    },
    wrapper: {
        height: "100%",
        width: '100%',
        // alignItems: 'center',
    },
    brand: {
        // paddingTop:"30%",
        // flexGrow:1,
        flex: 1,
        backgroundColor: colors.darkBlue,
        width: '100%',
        paddingHorizontal:-10,
        // height:'100%',
        // overflow: 'hidden',
        // blurRadius:6
        // borderWidth:1,

    },
    text: {
        textAlign: 'center',
        fontSize: 18,
        color: colors.white,
        paddingTop: 17,
        width: 110,
        fontFamily: 'Rubik-Bold',
        // borderWidth:1
    },
    text1: {
        color: colors.white,
        fontSize: 35,
        width: 224,
        textAlign: 'center',
        // fontWeight:'bold',
        fontFamily: 'Rubik-Medium',
        paddingTop: '25%',
        // backgroundColor: 'transparent'
        // borderWidth:1,
    },
    backButton: {
        // alignItems:'center'
        paddingLeft: 20,
    },
    view: {
        flex: 1,
        paddingTop: 40,
        paddingHorizontal: 30,
        paddingBottom:40,
        marginBottom:0,
        // marginTop:'75%',
        marginTop:Height*0.28,

        backgroundColor: colors.white,
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        zIndex: 1,
        // borderWidth: 1,
    },
    footer: {
        textAlign: 'center',
        // paddingTop:0.08*Height,
        position: 'absolute',
        bottom: 20,
        left: '44%',
        fontFamily: 'Rubik-Regular',
        color: colors.footerColor
    },
    row: {

        // borderWidth:1,
        paddingBottom: 10,
        paddingLeft: 10,
        marginTop:statusBarHeigth,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'transparent',
        alignItems: 'center'
    },
    title: {
        width: '85%',
        color: colors.white,
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        // borderWidth:1,
        textAlign: 'center',
    },
    touchable: {
        // borderWidth:1,
        paddingHorizontal: 10,

    },
    wrappers: {
        // flex:1,
        flexGrow: 1,
        // width: '100%',


        // borderWidth:1,
        // paddingVertical: 16
    },
    error: {
        color: '#e00a13',
        fontSize: 12,
        paddingBottom: 15,
        // marginHorizontal:'9%'
    },
    text2: {
        fontSize: 12,
        color: colors.font,
        paddingBottom: 20,
    }


})

export default styles
