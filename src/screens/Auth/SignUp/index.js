import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ImageBackground,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import * as Animatable from 'react-native-animatable';


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines, createRequest} from "services/api";
import {get, toArray} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from 'react-native-vector-icons/MaterialIcons'
import {image, saveUser, tex_pass} from 'services/actions/index'

// -----SVG----
import BackButton from 'assets/image/SVG/BackButton'
import LinearGradient from "react-native-linear-gradient";
import Translate from "../../../services/translateTitle";
import OneSignal from "react-native-onesignal";

let bottomSheet = null;


class Login extends Component {

    constructor(props) {
        super(props);
        this.onIds = this.onIds.bind(this);
        OneSignal.addEventListener('ids', this.onIds);
    }

    state = {
        file: {},
        isChecked: true,
        avtoType_uz: [
            {label: 'Kamaz', value: 'Kamaz'},
            {label: 'car 2', value: 'name2'},
            {label: 'car 3', value: 'name3'},
            {label: 'car 4', value: 'name4'},
        ],
        pid: ''
    }

    componentDidMount() {
        this.PickerData()

    }

    componentWillUnmount() {
        OneSignal.removeEventListener('ids', this.onIds);
    }

    onIds(device) {
        console.log('Device info: ', device);
        console.log(device.userId);
        this.setState({
            pid: device.userId,
        })
        this.props.setFieldValue('oneID', device.userId)
    }

    // Selected = (value) => {
    //     // this.setState({carType:value})
    //     this.props.setFieldValue('carType', value)
    // }

    PickerData = (id) => {
        // this.setState({search});
        Routines.auth.getPickerData({
            request: {},
        }, this.props.dispatch)
            .then((data) => {
                    // this.getItems(1)
                    // NavigationService.goBack()
                },
            )
            .catch(e => {
                // if(e.message === 'NETWORK_ERROR'){
                //     ToastAndroid.show(changeLanguage['tarmoq'], ToastAndroid.SHORT)
                //     // NavigationService.goBack()
                // }
            })
    };


    bs = React.createRef();
    ts = React.createRef();
    fall = new Animated.Value(1);
    takePhotoFromCamera = () => {
        ImagePicker.openCamera({
            // compressImageMaxWidth: 300,
            // compressImageMaxHeight: 300,
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(image(file));
            this.props.setFieldValue('file', file)
            this.bs.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(image({}));
        });
    }
    choosePhotoFromLibrary = () => {
        ImagePicker.openPicker({
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(image(file));
            this.props.setFieldValue('file', file)
            // this.setState({file: image})
            this.bs.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(image({}));
        });

    }
    takePhotoFrom = () => {
        ImagePicker.openCamera({
            // compressImageMaxWidth: 300,
            // compressImageMaxHeight: 300,
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(tex_pass(file));
            this.props.setFieldValue('tex_pass', file)
            this.ts.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(tex_pass({}));
        });
    }
    choosePhotoFromLib = () => {
        ImagePicker.openPicker({
            width: 1080,
            height: 800,
            mediaType: 'photo',
            cropping: true,
            compressImageQuality: 0.7
        }).then(file => {
            console.log(file);
            this.props.dispatch(tex_pass(file));
            this.props.setFieldValue('tex_pass', file)
            // this.setState({file: image})
            this.ts.current.snapTo(1);
        }).catch(e => {
            this.props.dispatch(tex_pass({}));
        });

    }

    renderInner = () => (
        <View style={styles.panel}>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.panelTitle}>Upload Photo</Text>
                <Text style={styles.panelSubtitle}/>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={this.takePhotoFromCamera}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={this.choosePhotoFromLibrary}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => this.bs.current.snapTo(1)}>
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );
    renderIn = () => (
        <View style={styles.panel}>
            <View style={{alignItems: 'center'}}>
                <Text style={styles.panelTitle}>Upload Photo</Text>
                <Text style={styles.panelSubtitle}/>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={this.takePhotoFrom}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={this.choosePhotoFromLib}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => this.ts.current.snapTo(1)}>
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );

    renderHeader = () => (
        <View style={styles.header}>
            <View style={styles.panelHeader}>
                <View style={styles.panelHandle}/>
            </View>
        </View>
    );
    Selected = (value, id) => {
        if (id === 1) {
            this.props.setFieldValue('carType', value)
        } else {
            console.log("dsds:", value)
            this.props.setFieldValue('location',value)
        }

        console.log("VALUE_S:", id)
    }

    objectToArray = (object) => {
        const array = toArray(object)
        const obj = [{
            label: '',
            value: ''
        }]
        array.map((item, index) => {
            // console.log("b", item)
            obj[index] = {
                label: item,
                value: item
            }
        })
        console.log("OBJECT:", obj)
        return obj
    }


    render() {

        const {handleSubmit, errors, t, image, tex_pass, getPickerData, currentLangCode, route} = this.props
        let {item1, isChecked} = this.state
        console.log("img:", tex_pass)
        console.log("phone:", route.params.phone)
        console.log("password:", route.params.password)
        console.log("ToArray:", toArray(getPickerData.avtoType_uz))

        return (
            <>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>

                <BottomSheet
                    ref={this.bs}
                    snapPoints={['43%', 0]}
                    renderContent={this.renderInner}
                    renderHeader={this.renderHeader}
                    initialSnap={1}
                    callbackNode={this.fall}
                    enabledGestureInteraction={true}
                    enabledInnerScrolling={false}
                    callbackThreshold={0.9}
                />
                <BottomSheet
                    ref={this.ts}
                    snapPoints={['43%', 0]}
                    renderContent={this.renderIn}
                    renderHeader={this.renderHeader}
                    initialSnap={1}
                    callbackNode={this.fall}
                    enabledGestureInteraction={true}
                    enabledInnerScrolling={false}
                    callbackThreshold={0.9}
                />
                <ImageBackground
                    // source={require('assets/image/tracker.png')}
                    style={styles.brand}
                    resizeMode={'cover'}
                    blurRadius={1}
                >
                    <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                    style={styles.wrapper}>
                        <View style={styles.row1}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton} color={colors.white}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('reg')}
                            </Text>
                        </View>
                        <Components.AnimatedButton/>


                        <Animatable.View style={styles.view}
                                         animation="fadeInUpBig"
                        >
                            <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                                     contentContainerStyle={styles.wrappers}>
                                <Field
                                    name={'name'}
                                    label={'name'}
                                    title={`${t('FIO')}*`}
                                    placeHolder={'Andrey'}
                                    component={Components.InputText}

                                />
                                {errors.name ? (
                                    <Text style={styles.error}>{errors.name}</Text>
                                ) : null}
                                <Components.Picker title={`${t('car_type')}*`}
                                                   zIndex={5000}
                                                   items={Translate.title(currentLangCode, getPickerData.avtoType_ru, getPickerData.avtoType_uz)}
                                                   placeholder={'Car'}
                                                   select={(value) => this.Selected(value,1)}


                                />
                                {errors.carType ? (
                                    <Text style={styles.error}>{errors.carType}</Text>
                                ) : null}

                                <Components.Picker
                                    title={`${t('manzil')}*`}
                                    // title={t('manzil')}
                                    zIndex={3000}
                                    items={Translate.title(currentLangCode, this.objectToArray(getPickerData.location_ru), this.objectToArray(getPickerData.location_uz))}
                                    placeholder={t('address')}
                                    select={(value) => this.Selected(value, 2)}

                                />
                                {errors.location ? (
                                    <Text style={styles.error}>{errors.location}</Text>
                                ) : null}

                                <Field
                                    name={'number'}
                                    label={'number'}
                                    title={`${t('Государственный номер транспорта ')}*`}
                                    placeHolder={'01 255 AAA'}
                                    // mask={"[00] [000] [AAA]"}
                                    component={Components.InputText}
                                />
                                {errors.number ? (
                                    <Text style={styles.error}>{errors.number}</Text>
                                ) : null}

                                <Field
                                    name={'volume'}
                                    label={'volume'}
                                    title={`${t('volume')}*`}
                                    placeHolder={'Пример: 800 м3.'}
                                    component={Components.InputText}
                                    keyboards={'number-pad'}
                                />
                                {errors.volume ? (
                                    <Text style={styles.error}>{errors.volume}</Text>
                                ) : null}

                                <Field
                                    name={'mass'}
                                    label={'mass'}
                                    title={`${t('mass')}*`}
                                    placeHolder={'Пример: 800 т.'}
                                    component={Components.InputText}
                                    keyboards={'number-pad'}
                                />
                                {errors.mass ? (
                                    <Text style={styles.error}>{errors.mass}</Text>
                                ) : null}
                                {/*<View style={styles.rows}>*/}
                                {/*    <TouchableOpacity style={styles.row} onPress={() => {*/}
                                {/*        this.props.setFieldValue('isChecked', isChecked)*/}
                                {/*        !isChecked ? this.setState({isChecked: !isChecked}) : null*/}
                                {/*    }}>*/}
                                {/*        {isChecked ? <Icon name={'check-box'} size={25} color={colors.green}/> :*/}
                                {/*            <Icon name={'check-box-outline-blank'} size={25} color={colors.green}/>}*/}
                                {/*        <Text style={styles.checkLabel}>*/}
                                {/*            Тонна*/}
                                {/*        </Text>*/}
                                {/*    </TouchableOpacity>*/}
                                {/*    <TouchableOpacity style={styles.row} onPress={() => {*/}
                                {/*        this.props.setFieldValue('isChecked', isChecked)*/}
                                {/*        isChecked ? this.setState({isChecked: !isChecked}) : null*/}
                                {/*    }}>*/}

                                {/*        {!isChecked ? <Icon name={'check-box'} size={25} color={colors.green}/> :*/}
                                {/*            <Icon name={'check-box-outline-blank'} size={25} color={colors.green}/>}*/}
                                {/*        <Text style={styles.checkLabel}>*/}
                                {/*            м3*/}
                                {/*        </Text>*/}
                                {/*    </TouchableOpacity>*/}
                                {/*</View>*/}


                                <Components.DownloadButton text={t('passport')}
                                                           color={colors.text}
                                                           background={colors.white}
                                                           border={1}
                                                           touch={() => {
                                                               this.bs.current.snapTo(0)
                                                           }}
                                />
                                {errors.file ? (
                                    <Text style={styles.error}>{errors.file}</Text>
                                ) : null}
                                <Components.DownloadButton text={t('tex_passport')}
                                                           color={colors.text}
                                                           background={colors.white}
                                                           border={1}
                                                           touch={() => {
                                                               // this.props.setFieldValue('file', image)
                                                               this.ts.current.snapTo(0)
                                                           }}
                                />

                                <Components.Button text={t('continue')}
                                                   color={colors.white}
                                                   background={colors.green}
                                                   padding={'5%'}
                                                   touch={() => {
                                                       handleSubmit()
                                                   }}
                                                   isActive={this.props.isSubmitting}

                                />
                                <Text style={styles.footer}>
                                    Powered by Soft Plus
                                </Text>
                            </KeyboardAwareScrollView>
                        </Animatable.View>


                    </LinearGradient>
                </ImageBackground>
            </>
        )
    }
}

Login = withFormik({
    mapPropsToValues: () => ({
        name: "",
        volume: "",
        mass: "",
        // transportType: "",
        file: {},
        isChecked: true,
        tex_pass: {},
        number: '',
        carType: '',
        oneID: '',
        location:''
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            name: Yup.string().required(t("required")),
            carType: Yup.string().required(t("required")),
            location: Yup.string().required(t("required")),
            volume: Yup.string().required(t("required")),
            mass: Yup.string().required(t("required")),
            number: Yup.string().required(t("required"))
                .min(8, '8 belgidan kam kiritmang !')
                .max(8, '8 belgidan kop kiritmang !'),
            // transportType: Yup.string().required(t("required")),
            file: Yup.object().required(t("required")),
            tex_pass: Yup.object().required(t("required"))
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log('value:', values);
        let {name, carType, file, tex_pass, location, number, volume, mass, oneID} = values;
        let {image, route} = props
        let {phone, password} = route.params
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // let {file} = props
        // console.log("IMAGE_UPLOAD: ", file)
        // console.log("IMAGE_UPLOAD2: ", tex_pass)
        setSubmitting(true);

        let formData = new FormData()
        console.log("FORMDATA:", formData)

        const parts = file.path.split('/');
        const pass = tex_pass.path.split('/');

        formData.append(`passportPhoto`, {
            name: file.name ? file.name : parts[parts.length - 1],
            filename: file.filename ? file.filename : parts[parts.length - 1],
            type: file.type ? file.type : file.mime,
            uri: file.sourceURL ? file.sourceURL : file.path
        });
        formData.append(`techPassportPhoto`, {
            name: tex_pass.name ? tex_pass.name : pass[pass.length - 1],
            filename: tex_pass.filename ? tex_pass.filename : pass[pass.length - 1],
            type: tex_pass.type ? tex_pass.type : tex_pass.mime,
            uri: tex_pass.sourceURL ? tex_pass.sourceURL : tex_pass.path
        });
        formData.append("fullname", name)
        formData.append("phonenumber", phone)
        formData.append("password", password)
        formData.append("transportType", carType)
        formData.append("transportGovNumber", number)
        formData.append("baggageVolume", volume)
        formData.append("baggageMass", mass)
        formData.append("oneID", oneID)
        formData.append("location", location)


        Routines.auth.signUp({
            request: {
                data: formData
            }
        }, props.dispatch)
            .then((data) => {
                setSubmitting(false);

                if (get(data.response, 'data.success', false)) {
                    props.dispatch(saveUser(data.response.data))
                    NavigationService.navigate('drawer')
                    return (
                        Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
                    )

                } else {
                    console.log("Success")
                }
            })
            .catch(e => {
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                setSubmitting(false);

                // setSubmitting(false);
                // if (e.message === "NETWORK_ERROR") {
                // }
            });
    }
})(Login);
const mapStateToProps = (state, ownProps) => {
    return {
        file: state.file,
        image: state.user.image,
        tex_pass: state.user.tex_pass,
        currentLangCode: state.language.lang,
        getPickerData: state.profile.getPickerData,


    };
};
Login = connect(mapStateToProps)(Login)
export default withTranslation('main')(Login)
