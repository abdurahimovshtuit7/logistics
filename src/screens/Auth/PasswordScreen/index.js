import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput, ImageBackground,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import * as Animatable from 'react-native-animatable';


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import LottieView from 'lottie-react-native';


// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";

// -----SVG----
import Track from 'assets/image/SVG/Track'
import BackButton from 'assets/image/SVG/BackButton'
import {HeaderBackButton} from "@react-navigation/stack";
import LinearGradient from "react-native-linear-gradient";

class Password extends Component {
    state = {
        icon1: 'eye-off',
        icon2: 'eye-off',
        password1: true,
        password2: true,
    }
    _changeIcon1 () {
        this.setState(prevState => ({
            icon1: prevState.icon1 === 'eye' ? 'eye-off' : 'eye',
            password1: !prevState.password1,
        }))
    }
    _changeIcon2 () {
        this.setState(prevState => ({
            icon2: prevState.icon2 === 'eye' ? 'eye-off' : 'eye',
            password2: !prevState.password2,
        }))
    }

    render() {
        const {handleSubmit, errors,t,route} = this.props
        const {phone} = route.params
        console.log("Phone:",route.params.phone)
        return (
            // <SafeAreaView style={styles.container}>
            <Components.Layout>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>
                {/*<LottieView source={require('../../../assets/image/lottie/data-security.json')} autoPlay loop />;*/}

                <ImageBackground
                    // source={require('assets/image/tracker.png')}
                                 style={styles.brand}
                                 resizeMode={'cover'}
                                 blurRadius={1}
                    // resizeMethod={'resize'}
                >
                    <LinearGradient colors={[colors.linearBegin, colors.linearEnd]}
                                    style={styles.wrapper}>
                        <View style={styles.row}>
                            <TouchableOpacity style={styles.touchable} onPress={() => {
                                NavigationService.goBack()
                            }}>
                                <BackButton style={styles.backButton} color={colors.white}/>
                            </TouchableOpacity>

                            <Text style={styles.title}>
                                {t('reg')}
                            </Text>

                        </View>

                        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled"
                                                 enableOnAndroid={false}
                                                 enableResetScrollToCoords={false}
                                                 contentContainerStyle={styles.wrappers}>
                            <Animatable.View style={styles.view}
                                             animation = "fadeInUpBig">
                                <Field
                                    name={'password'}
                                    label={'password'}
                                    title={t('parol')}
                                    placeHolder={t('parol')}
                                    component={Components.SecureInput}
                                    secureTextEntry={this.state.password1}
                                    changeIcon={()=>this._changeIcon1()}
                                    iconName={this.state.icon1}
                                    // mask={"(+998) [00] [000] [00] [00]"}
                                />
                                {errors.password ? (
                                    <Text style={styles.error}>{errors.password}</Text>
                                ) : null}
                                <Field
                                    name={'confirmPassword'}
                                    label={'confirmPassword'}
                                    title={t('password_confirm')}
                                    placeHolder={t('password_confirm')}
                                    component={Components.SecureInput}
                                    secureTextEntry={this.state.password2}
                                    changeIcon={()=>this._changeIcon2()}
                                    iconName={this.state.icon2}
                                    // mask={"(+998) [00] [000] [00] [00]"}
                                />
                                {errors.confirmPassword ? (
                                    <Text style={styles.error}>{errors.confirmPassword}</Text>
                                ) : null}

                                <Components.Button text={t('reg')}
                                                   color={colors.white}
                                                   background={colors.green}
                                                   padding={20}
                                                   touch={() => {
                                                       handleSubmit()
                                                   }}
                                />
                                <Text style={styles.footer}>
                                    Powered by Soft Plus
                                </Text>
                            </Animatable.View>
                        </KeyboardAwareScrollView>
                    </LinearGradient>
                </ImageBackground>

            </Components.Layout>
            // {/*</SafeAreaView>*/}
        )
    }
}

Password = withFormik({
    mapPropsToValues: () => ({
        password: "",
        confirmPassword:''

    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            password: Yup.string()
                .required("Parolni kiriting!")
                .min(8,'8 belgidan kam kiritmang !'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required("Parolni kiriting!")
                .min(8,'8 belgidan kam kiritmang !'),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);
        let {name, email, password, confirmPassword} = values;
        let {phone} = props.route.params
        let onlyNums = phone.replace(/[^\d]/g, '');
        NavigationService.navigate('signUp',{phone:onlyNums,password:password})

        // let {route} = this.props
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)
        // setSubmitting(true);
        // Routines.auth.signUp({
        //     request: {
        //         data: {
        //             name: name,
        //             phone:phone,
        //             email:email,
        //             type:'personal',
        //             password: password,
        //             confirmPassword:confirmPassword
        //         }
        //     }
        // }, props.dispatch)
        //     .then((data) => {
        //         if(get(data.response,'data.success',false)){
        //             NavigationService.navigate('home')
        //             return (
        //                 Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
        //             )
        //
        //         } else {
        //             console.log("Success")
        //         }
        //     })
        //     .catch(e => {
        //         // setSubmitting(false);
        //         // if (e.message === "NETWORK_ERROR") {
        //         // }
        //     });
    }
})(Password);
const mapStateToProps = (state, ownProps) => {
    return {};
};
Login = connect(mapStateToProps)(Password)
export default withTranslation('main')(Password)
