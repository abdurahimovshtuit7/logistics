import {StyleSheet, Platform} from 'react-native';
import colors from 'assets/styles/colors'
import { Dimensions } from 'react-native';

const styles = StyleSheet.create({
   container:{
       flex:1,
       alignItems: 'center',
       // justifyContent: 'center',
       backgroundColor:colors.darkBlue
   },
    brandName:{
        fontSize:29,
        color:colors.white,
        marginTop:'80%',
        fontFamily:'Rubik-Bold'
    },
    footer:{
        fontSize: 12,
        color:colors.footer,
        position:'absolute',
        bottom:31,
        fontFamily:'Rubik-Regular'
    },
    loading:{
        fontSize:14,
        color:colors.white,
        marginTop:9,
        fontFamily:'Rubik-Regular'
    }

})
export default styles
