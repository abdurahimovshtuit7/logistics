import React, {Component} from 'react';
import {connect} from 'react-redux';
import NavigationService from 'navigators/NavigationService';
import Launch from './index_without_connect';
import i18next from 'i18next';
import {CommonActions} from '@react-navigation/native';

import {Platform, StatusBar} from 'react-native';
import colors from 'assets/styles/colors';
import {get} from "lodash";
import Components from "../../../components";

class LaunchScreen extends Component {
    getData() {
        setTimeout(() => {
            if(get(this.props.userData, 'token', '')){
                if(get(this.props.userData, 'role', '') === 'driver'){
                    this.props.navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'drawer'},
                            ],
                        }),
                    );
                }
                else { this.props.navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'admin'},
                        ],
                    }),
                );  }
            }
            else this.props.navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {name: 'auth'},
                    ],
                }),
            );

        }, 200);
    }

    componentDidMount() {
        this.getData();
        i18next.changeLanguage(this.props.currentLangCode);
    };

    render() {
        // console.log("Render:",this.props.navigation)
        return (
            <>
                <StatusBar backgroundColor={colors.darkBlue} barStyle="light-content" translucent={false}/>
                <Launch/>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode: state.language.lang,
        userData: state.user.user,
    };
};
export default connect(mapStateToProps)(LaunchScreen);
