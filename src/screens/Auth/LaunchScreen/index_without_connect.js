import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
    View,
    Text,
    ActivityIndicator,
    Platform
} from 'react-native'

import styles from './styles'


class LaunchScreen extends Component {

    render () {
        // console.warn(JSON.stringify(this.props.login))
        return (
            <View style={styles.container}>
                <Text style={styles.brandName}>
                    Logen Berg
                </Text>
                <Text style={styles.loading}>
                    Loading
                    {/*<AnimatedEllipsis numberOfDots={3}*/}
                    {/*                  minOpacity={0.4}*/}
                    {/*                  style={{*/}
                    {/*                      color: '#fff',*/}
                    {/*                      fontSize: 10,*/}
                    {/*                      marginTop:20,*/}
                    {/*                      letterSpacing: -5,*/}
                    {/*                  }}/>*/}
                </Text>
                {/*<Text style={styles.footer}>*/}
                {/*    Powered by © UMDSOFT*/}
                {/*</Text>*/}
            </View>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {

    }
}
export default connect(mapStateToProps)(LaunchScreen)
