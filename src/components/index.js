import Layout from './Layout'
import Button from './Button'
import InputText from './InputText'
import AnimatedButton from "./animatedButton";
import DownloadButton from './DownloadButton'
import StatusButton from './StatusButton'
import Item from './Item'
import Picker from "./Picker";
import ButtonPicker from './ButtonPicker'
import SelectLanguage from './SelectLanguage'
import Description from './Description'
import ImageViewer from './ImageViewer'
import AnimationButton from './AnimationButton'
import Input from './Input'
import SecureInput from './SecureInput'
import InfiniteFlatList from "./FlatListInfinity";
import ImageUpload from './ImageUpload'
import Toast from './Toast'
import Select from "./Select";
import SelectLang from './SelectLang'


export default {
    Layout,
    Button,
    InputText,
    AnimatedButton,
    DownloadButton,
    StatusButton,
    Item,
    Picker,
    ButtonPicker,
    SelectLanguage,
    Description,
    ImageViewer,
    Input,
    SecureInput,
    AnimationButton,
    InfiniteFlatList,
    ImageUpload,
    Toast,
    Select,
    SelectLang,
}
