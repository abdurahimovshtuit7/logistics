import React, {Component, useState} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Platform
} from "react-native";
import styles from './styles'
import colors from "assets/styles/colors";
//import NavigationService from "../../navigators/NavigationService";
import PropTypes from 'prop-types'
import DropDownPicker from "react-native-dropdown-picker";
import Components from "../index";
// import TextInputMask from "react-native-text-input-mask";


// const stylesFunction = (focus, error) => {
//     console.log("Errorrr:", error)
//     if (focus && !error) {
//         return styles.focusedTextInput
//     } else if (error) {
//         return styles.error
//     } else return styles.textInput
// }
const  changeVisibility = (setState) => {
    setState(false);
}

const Picker = (props) => {
    const [isVisible, setState] = useState(false)
    const [item, setItem] = useState(null)

    return (
        <>
            <Text style={styles.title}>
                {props.title}
            </Text>
            <DropDownPicker
                zIndex={props.zIndex}
                items={props.items}
                placeholder={props.placeholder}
                style={styles.component}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                dropDownStyle={[styles.dropDownStyle]}
                defaultValue={item}
                // containerStyle={styles.component}
                isVisible={isVisible}
                onOpen={() => changeVisibility(setState)}
                onClose={() => setState( false)}
                onChangeItem={item =>{setItem(item.value),props.select(item.value)}}
            />
        </>
    )
}
Picker.propTypes = {
    style: PropTypes.object,
    containerStyle: PropTypes.object,
    title: PropTypes.string,
    zIndex:PropTypes.number,
    items:PropTypes.array,
    placeholder:PropTypes.string,
    select:PropTypes.func,

};
Picker.defaultProps = {
    style: {},
    containerStyle: {},
    title: '',
    zIndex:1,
    items:[],
    placeholder:'',
    select:function () {},

};
export default Picker
