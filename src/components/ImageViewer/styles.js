import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        // zIndex:2,
        marginVertical:7.5,
        marginHorizontal:20,
        backgroundColor: colors.white,
        borderRadius:5,
        paddingHorizontal:12,
        paddingVertical:15,
    },
    image:{
        height:200,
        width:'100%'
        // paddingHorizontal: 10,
    },
    title:{
        fontFamily:'Rubik-Medium',
        fontSize:14,
        color:colors.black,
        paddingBottom:16
    },
    text:{
        textAlign:'center',
        color:colors.grey,
        paddingVertical: 10
    }


})

export default styles
