import React from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import Lightbox from 'react-native-lightbox-v2';
import colors from "assets/styles/colors";
import Download from 'assets/image/SVG/Download'
// import Arrow from 'assets/image/SVG/arrow'

const renderCarousel = () => (
        <Image
            style={{ flex: 1 }}
            resizeMode="contain"
            source={require('assets/image/tex.png')}
        />
)

const Submit = (props) => {
    const {data} = props

    const imageOne = `http://195.158.2.207/uploads/${data.techPassportPhoto}`
    const imageTwo = `http://195.158.2.207/uploads/${data.passportPhoto}`
    return  (
        <View style={[styles.container,props.style]}>
            <Text style={styles.title}>
                {props.doc}
            </Text>
            <Lightbox underlayColor="transparent"
                // springConfig={{tension: 5, friction: 13}}
                // swipeToDismiss={false}
                // renderContent={renderCarousel}
                      backgroundColor={colors.scale}>
                <Image style={styles.image}
                       resizeMode={'contain'}
                    // resizeMethod={'resize'}
                       source={{uri:imageOne}}>

                </Image>
            </Lightbox>
            <Text style={styles.text}>
                {props.tex}
            </Text>
            <Lightbox underlayColor="transparent"
                // springConfig={{tension: 5, friction: 13}}
                // swipeToDismiss={false}
                      backgroundColor={colors.scale}>
                <Image style={styles.image}
                       resizeMode={'contain'}
                       source={{uri:imageTwo}}>

                </Image>
            </Lightbox>
            <Text style={styles.text}>
                {props.pass}
            </Text>

        </View>
    );
}

Submit.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    background:PropTypes.string,
    color:PropTypes.string,
    border:PropTypes.number,
    icon:PropTypes.bool,
    margin:PropTypes.number,
    doc:PropTypes.string,
    tex:PropTypes.string,
    pass:PropTypes.string,
    data:PropTypes.object
    // margin:PropTypes.string,

};
Submit.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
    background:'',
    color:'',
    border:null,
    icon:true,
    margin:0,
    doc:'',
    tex:'',
    pass:'',
    data:{}

    // margin:''
};
export default Submit;
