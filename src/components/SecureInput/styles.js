import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    textInput: {
        // flexDirection:'row',
        paddingVertical:10,
        justifyContent:'center',
        fontSize: 14,
        width:'90%',
        marginBottom:5,
        // borderWidth: 1,
    },
    row:{
        flexDirection: 'row',
        // paddingVertical:8,
        alignItems:'center',
        // borderWidth: 1,
        // width: '0%',
        justifyContent: 'space-between'
    },
    icon:{
      width: '10%'
    },

    container:{
        // marginHorizontal:'9%',
        backgroundColor:colors.white,
        paddingVertical: 21,
        // width:'80%',
        // borderWidth:1
    },
    title:{
        fontSize:14,
        fontFamily:'Rubik-Medium',
        color:colors.black,
        paddingBottom:11,
    },
    text:{
        paddingLeft:10,
        fontSize:16,
        fontWeight:'500'
    },
    border:{
        borderWidth:0.7,
        flex:1,
        borderColor:colors.green,
        // marginBottom:'13%'
    },

})

export default styles
