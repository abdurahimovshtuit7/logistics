import React, {Component, useState} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    TextInput
} from "react-native";
import styles from './styles'
import colors from "assets/styles/colors";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

//import NavigationService from "../../navigators/NavigationService";
import PropTypes from 'prop-types'
import TextInputMask from "react-native-text-input-mask";

const Input = (props) => {
    const [hasFocus, setState] = useState(false)
    // const setFocus (hasFocus) {
    //     setState({hasFocus});
    // }
    const [icon, setIcon] = useState('eye-off')
    const [password, changePassword] = useState('true')
    let {placeHolder, onChangeText, onBlur, value, field, form, hasErrorMessage, keyboards, mask} = props
    let error = form && form.touched[field.name] && form.errors[field.name]
    let err = form.touched[field.name] && form.errors[field.name]

    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                {props.title}
            </Text>
            <View style={styles.row}>
                <TextInputMask
                    style={styles.textInput}
                    clearButtonMode={'unless-editing'}
                    secureTextEntry={props.secureTextEntry}
                    placeholder={placeHolder}
                    placeholderTextColor={colors.textGray}
                    underlineColorAndroid="#fff"
                    autoCapitalize='none'
                    onChangeText={form.handleChange(field.name)}
                    // onBlur={()=>{form.handleBlur(field.name), setState(false)
                    // }}
                    value={field.value}
                    keyboardType={keyboards}
                    mask={props.mask}
                    // autoCorrect={false}
                >
                </TextInputMask>
                {/*<Text style={styles.icon}>h</Text>*/}
                <Icon style={styles.icon} name={props.iconName} onPress={props.changeIcon} size={25} color={colors.green}/>
                {/*{props.icon}*/}
            </View>

            <View style={styles.border}/>
        </View>
    )
}
Input.propTypes = {
    style: PropTypes.object,
    containerStyle: PropTypes.object,
    hasErrorMessage: PropTypes.bool,
    hasLeftText: PropTypes.bool,
    hasUnderLine: PropTypes.bool,
    leftTextStyle: PropTypes.object,
    hairLineStyle: PropTypes.object,
    leftText: PropTypes.string,
    hasRightText: PropTypes.bool,
    rightTextStyle: PropTypes.object,
    rightText: PropTypes.string,
    title: PropTypes.string,
    mask: PropTypes.string,
    icon: PropTypes.object,
    secureTextEntry: PropTypes.bool,
    iconName:PropTypes.string,
    changeIcon:PropTypes.func,

};
Input.defaultProps = {
    style: {},
    containerStyle: {},
    hairLineStyle: {},
    hasErrorMessage: false,
    hasUnderLine: true,
    hasLeftText: false,
    leftTextStyle: {},
    leftText: '',
    hasRightText: false,
    rightTextStyle: {},
    rightText: '',
    title: '',
    mask: '',
    icon: {},
    secureTextEntry: false,
    iconName:'',
    changeIcon:()=>{}
}
;
export default Input
