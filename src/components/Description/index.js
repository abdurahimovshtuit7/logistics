import React, {useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
import Car from 'assets/image/SVG/Car'
import Check from 'assets/image/SVG/Check'

const Item = (props) => {
    const [value, setValue] = useState('1')
    const {data} = props
    return (
        <View style={styles.view} >
            <View style={styles.circle}/>
            <View style={styles.content}>
                <View style={styles.row}>
                    <Text style={styles.text1} numberOfLines={2}>{data.fullname}</Text>
                    <View style={styles.weight}>
                        <Car/>
                        <Text style={styles.volume} numberOfLines={1}>{data.baggageVolume } m3</Text>
                    </View>
                    {
                        (data.baggageMass) ? (<View style={styles.weight2}>
                            <Text style={styles.volume} numberOfLines={1}>{data.baggageMass} t</Text>
                        </View>) : (<View style={styles.weight2}>
                            <Text style={styles.volume} numberOfLines={1}> ...</Text>
                        </View>)
                    }

                    <View>
                        <Check color={data.status==="Свободен"?colors.brandGreen:colors.red}/>
                    </View>
                </View>
                <View style={styles.row1}>
                    <View style={styles.circle1}/>
                    <Text style={styles.carType}>
                        {data.transportType} | {data.transportGovNumber}
                    </Text>

                </View>
                <View style={styles.row2}>
                        <Text style={styles.city}>
                            {data.location}
                        </Text>
                        {
                           data.status === "Свободен" ? (
                                <Text style={styles.time}>
                                    {data.status}
                                </Text>
                            ) : (<Text style={styles.time}>
                                {data.timeOfStatus}
                            </Text>)
                        }
                </View>
                <View style={styles.border}/>
                <View style={styles.footer}>
                    <Text style={styles.title}>
                        Номер телефона
                    </Text>
                    <Text style={styles.phone}>
                        {data.phonenumber}
                    </Text>
                </View>
            </View>

        </View>)
};
Item.propTypes = {
    style: PropTypes.object,
    // PROP: PropTypes.array,
    onSelected: PropTypes.func,
    carType:PropTypes.string,
    carNumber:PropTypes.string,
    name:PropTypes.name,
    touch:PropTypes.func,
    data:PropTypes.object

};
Item.defaultProps = {
    style: {},
    // PROP: [],
    onSelected: function () {
    },
    carNumber:'',
    carType:'',
    name:'',
    touch:function () {},
    data:{}
};
export default Item;
