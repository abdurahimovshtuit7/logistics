import { StyleSheet } from 'react-native';
import colors from "assets/styles/colors";
import {center, horizontal} from "assets/styles/commons";

export default StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingBottom: 30,
        alignItems: 'center',
        height: 258,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        backgroundColor: colors.white
    },
    line: {
        width: 60,
        height: 5,
        borderRadius: 20,
        backgroundColor: '#E6EAF0'
    },
    button: {
        paddingVertical: 17
    },
    buttonText: {
        fontSize: 16,
        // fontFamily: 'SFProDisplay-Medium',
        color: '#42495C'
    },
    bottomSheetContainer: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    }
});
