import React, { useRef, useEffect } from 'react';
import {
    TouchableOpacity,
    Text,
    View
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import {withTranslation} from "react-i18next";
import RBSheet from "react-native-raw-bottom-sheet";
import ImagePicker from 'react-native-image-crop-picker';
import Components from '../index'

const AddImage = ({ onImageSelected, getRef, t }) => {

    const RBSheetImage = useRef(null);
    getRef(RBSheetImage.current);

    return (
        <RBSheet
            onClose={()=>{
            }}
            ref={RBSheetImage}
            height={200}
            duration={250}
            closeOnDragDown
            customStyles={{ container: styles.bottomSheetContainer }}
        >

            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={()=>{
                    ImagePicker.openCamera({
                        width: 1080,
                        height: 1350,
                        compressImageQuality: 0.75,
                        mediaType: 'photo',
                        cropperToolbarTitle: t('Edit Photo'),
                        cropperToolbarColor: '#ffffff',
                        cropperStatusBarColor: '#000000',
                        cropping: true
                    }).then(image => {
                        console.log(image);
                        RBSheetImage.current.close();
                        onImageSelected(image)
                    })
                        .catch((e)=>{
                            if(e.code === "E_PERMISSION_MISSING"){
                                let text = t("The app does not have access to the camera You can change this in your settings");
                                RBSheetImage.close();
                                Components.Toast.show(text);
                            }
                        });
                }}>
                    <Text style={styles.buttonText}>
                        {t('Take a picture')}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={()=>{
                    ImagePicker.openPicker({
                        width: 1080,
                        height: 1350,
                        compressImageQuality: 0.75,
                        cropping: true,
                        mediaType: 'photo',
                        cropperToolbarTitle: t('Edit Photo'),
                        cropperToolbarColor: '#ffffff',
                        cropperStatusBarColor: '#000000',
                        multiple: false
                    }).then(image => {
                        console.log(image);
                        RBSheetImage.current.close();
                        onImageSelected(image)
                    })
                        .catch((e)=>{
                            if(e.code === "E_PERMISSION_MISSING"){
                                let text = t("Unable to access images Please allow access if you want to be able to select images.");
                                RBSheetImage.close();
                                Components.Toast.show(text);
                            }
                        });
                }}>
                    <Text style={styles.buttonText}>
                        {t('Choose from gallery')}
                    </Text>
                </TouchableOpacity>
            </View>

        </RBSheet>
    )
};
AddImage.propTypes = {
    onImageSelected: PropTypes.func,
    getRef: PropTypes.func
};
AddImage.defaultProps = {
    onImageSelected: ()=>{},
    getRef: ()=>{}
};
export default withTranslation("main")(AddImage);
