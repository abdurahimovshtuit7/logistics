import React, {useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
import Car from 'assets/image/SVG/Car'
import Check from 'assets/image/SVG/Check'

const Item = (props) => {
    const [value, setValue] = useState('1')
    return (
        <View style={styles.view}>
            <View style={styles.circle}/>
            <TouchableOpacity style={styles.content} onPress={props.touch}>
                <View style={styles.row}>
                    <Text style={styles.text1} numberOfLines={2}>{props.item.fullname}</Text>
                    <View style={styles.weight}>
                        <Car/>
                        <Text style={styles.volume} numberOfLines={1}>{props.item.baggageVolume} m3</Text>
                    </View>
                    {
                        (props.item.baggageMass) ? (<View style={styles.weight2}>
                            <Text style={styles.volume} numberOfLines={1}>{props.item.baggageMass} t</Text>
                        </View>) : (<View style={styles.weight2}>
                            <Text style={styles.volume} numberOfLines={1}> ...</Text>
                        </View>)
                    }

                    <View style={styles.check}>
                        <Check color={props.item.status === "Свободен" ? colors.brandGreen : colors.red}/>
                    </View>
                </View>
                <View style={styles.row1}>
                    <View style={styles.circle1}/>
                    <Text style={styles.carType}>
                        {props.item.transportType} | {props.item.transportGovNumber}
                    </Text>

                </View>
                <View style={styles.row2}>

                        <Text style={styles.city}>
                            {props.item.location}
                        </Text>

                    {/*<View style={styles.textRow}>*/}
                        {
                            props.item.status === "Свободен" ? (
                                <Text style={styles.time}>
                                    {props.item.status}
                                </Text>
                            ) : (<Text style={styles.time}>
                                {props.item.timeOfStatus}
                            </Text>)
                        }


                    {/*</View>*/}
                </View>
            </TouchableOpacity>

        </View>)
};
Item.propTypes = {
    style: PropTypes.object,
    // PROP: PropTypes.array,
    onSelected: PropTypes.func,
    carType: PropTypes.string,
    carNumber: PropTypes.string,
    name: PropTypes.name,
    touch: PropTypes.func,
    item: PropTypes.object

};
Item.defaultProps = {
    style: {},
    // PROP: [],
    onSelected: function () {
    },
    carNumber: '',
    carType: '',
    name: '',
    touch: function () {
    },
    item: {}
};
export default Item;
