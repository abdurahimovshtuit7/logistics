import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    view:{
        // marginTop:16,
        width:'100%',
        paddingHorizontal: 20,
        paddingVertical:10,
        // borderWidth:1,
        flexDirection:'row',
        // alignItems:'center',
        justifyContent:'space-between'
    },
    circle:{
        width:8,
        height:8,
        borderWidth: 2,
        borderRadius:20,
        borderColor:colors.whiteAlpha,
        marginTop:12,
        // marginRight:10,
    },
    content:{
        backgroundColor:colors.white,
        // width: '93%',
        borderRadius: 5,
        paddingVertical:12,
        paddingHorizontal: 15,
        //IOS
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems: 'center',
        // width:
        // borderWidth:1,
    },
    row1:{
        flexDirection: 'row',
        // justifyContent:'space-between',
        alignItems: 'center',
    },
    circle1:{
        width:10,
        height:10,
        borderWidth: 3,
        borderRadius:20,
        borderColor:colors.green,
        marginTop:3,
        marginRight: 3,
        // marginRight:10,
    },
    text1:{
        color:colors.black,
        fontFamily:'Rubik-Regular',
        paddingRight:3,
        fontSize:16,
        width:'42%',
        // borderWidth: 1,
    },
    text2:{
        color:colors.lightBlack,
        fontFamily:'Rubik-Regular',
        width:'70%',
        fontSize:14,

        // borderWidth: 1,
    },
    weight:{
        backgroundColor: colors.blue,
        flexDirection:'row',
        paddingVertical:3,
        paddingHorizontal:7,
        borderRadius:3,

        width:'26%',
        alignItems:'center'
    },
    weight2:{
        backgroundColor: colors.brandGreen,
        flexDirection:'row',
        paddingVertical:4,
        paddingHorizontal:7,
        borderRadius:3,
        width:'20%',
        // paddingVertical:
        alignItems:'center'
    },
    volume:{
        color:colors.white,
        paddingLeft:4,
        fontFamily:'Rubik-Bold',
        fontSize:12,
        // textAlign:'center'

    },
    carType:{
        color:colors.green,
        paddingHorizontal:3,
        borderRightColor:colors.green,
    },
    row2:{
        flexDirection: 'row',
        // borderWidth:1,
        paddingTop:'7%',
        justifyContent:'space-between',
        alignItems: 'center',
    },
    status:{
        color:colors.extraGrey,
        fontFamily:'Rubik-Italic',
        paddingRight: 4,
        fontSize:14,
    },
    textRow:{
      flexDirection:'row',
        // width:'60%'
    },
    time:{
        fontSize:14,
        width:'50%',
        textAlign:'right',
        fontFamily:'Rubik-BoldItalic',
        color:colors.timeColor
    },
    city:{
        fontSize:14,
        width:'43%',
        color:colors.black,
        fontFamily:'Rubik-Regular'
    },
    check:{
        // width:'17%'
    }
});

export default styles
