import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    textInput: {
        flexDirection:'row',
        paddingVertical:13,
        justifyContent:'center',
        fontSize: 14,
        // borderWidth: 1,
        // fontFamily: 'Poppins-Regular',
        paddingHorizontal:16,
        borderRadius:3,
        backgroundColor: colors.white,
    },
    // border:{
    //
    // }

    container:{
        // marginHorizontal:24,
        // backgroundColor:'rgba(255,255,255,0.61)',
        paddingVertical: 12,
        // borderWidth:1
    },
    title:{
        fontSize:14,
        fontFamily:'Rubik-Medium',
        color:colors.black,
        paddingBottom:11,
        // paddingBottom:8
    },
    red:{
       color: colors.red
    },
    text:{
        paddingLeft:10,
        fontSize:16,
        fontWeight:'500'
    },
    border:{
        borderWidth:0.7,
        flex:1,
        borderColor:colors.green,
        // marginBottom:'13%'
    },

})

export default styles
