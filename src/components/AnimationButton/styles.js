import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    view:{
        // marginTop:16,
        paddingHorizontal: 20
    },
    container: {
        // marginTop:27,
        // borderWidth:1,
        // marginHorizontal:28,
        borderRadius: 100,
        // borderColor: colors.border,
        marginVertical:12,
        // paddingVertical: 40,
        justifyContent:'center',
        alignItems:'center',
        width: '85%'
    },
    check:{
        marginVertical: 10,
        flexDirection:'row',
        // borderWidth:1,
        alignItems: 'center',
        borderRadius:5,
        backgroundColor: 'rgba(255, 255, 255, 0.08)'

    },
    circle:{
        width:25,
        height:25,
        borderRadius: 15,
        margin:8,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems:'center',
        borderColor: colors.brandGreen

    },
    circle1:{
        width:25,
        height:25,
        margin:8,
    },
    innerCircle:{
        width:9,
        height:9,
        backgroundColor:colors.brandGreen,
        borderRadius:10,
    },
    text:{
        fontSize:18,
        paddingVertical:21,
        // marginTop:15,
        fontFamily:'Rubik-Medium',
        // marginBottom:25,
        color:colors.black,
        // width:'90%'
        paddingHorizontal:20,
        alignSelf:'center'
    },



});

export default styles
