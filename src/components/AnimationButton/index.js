import React, {useEffect, useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text, ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
// import Person from 'assets/images/SVG/Person'
// import HandShake from 'assets/images/SVG/HandShake'

const Screen = (props) => {

    const [value, setValue] = useState('')
    useEffect(() => {
        // Update the document title using the browser API
        // if(props.status==="Свободен"){
        //     setValue('1')
        // }
        // else setValue('2')

    });
    return (
        <View style={[styles.view,{marginTop: props.margin}]}>

                    <View style={[styles.check,{
                        backgroundColor: (props.id  ? colors.background : 'transparent'),

                    }]}>
                        {
                            (props.id)?(
                                <View style={styles.circle}>
                                    <View style={styles.innerCircle}>

                                    </View>
                                </View>
                            ):(
                                <View style={styles.circle1}></View>
                            )
                        }

                    <TouchableOpacity
                                      activeOpacity={0.9}
                                      style={[styles.container, {
                                          backgroundColor: (props.id  ? props.color : colors.white),
                                          // borderWidth: (value === res.key ? null : 1)
                                      }]}
                                      onPress={() => {
                                          // setValue(res.key);
                                          props.onSelected()

                                      }}
                    >
                        {/*{res.key === '1'?(*/}
                        {/*    // <Person color={(value === res.key?colors.white:colors.black)}/>*/}
                        {/*):null*/}
                        {/*}*/}

                                <Text style={[styles.text,{color:(props.id ?colors.white:props.color)}]}>
                                    {props.title}
                                </Text>
                        {/*<Text style={[styles.text,{color:(value === res.key?colors.white:res.color)}]}>*/}
                        {/*    {res.title}*/}
                        {/*</Text>*/}


                    </TouchableOpacity>
                    </View>

        </View>)
};
Screen.propTypes = {
    style: PropTypes.object,
    PROP: PropTypes.array,
    onSelected: PropTypes.func,
    submit: PropTypes.func,
    margin:PropTypes.number,
    isActive:PropTypes.bool,
    status:PropTypes.string,
    id:PropTypes.string,
    color:PropTypes.string
};
Screen.defaultProps = {
    style: {},
    PROP: [],
    onSelected:function () {},
    submit:function () {},
    margin:0,
    isActive:false,
    status:'',
    id:'',
    color:colors.white
};
export default Screen;
