import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'
import DropDownPicker from "react-native-dropdown-picker";

const styles = StyleSheet.create({
    container: {
        paddingLeft: '58%',
        marginHorizontal:20,
        // borderWidth: 1,
        // width: '30%',
    },

    dropDownStyle: {
        // borderWidth:1,
        // borderTopWidth:1,
        marginLeft: '50%',
        marginHorizontal:20,
        borderWidth: 0,
        backgroundColor: 'transparent',
        shadowColor: 'rgba(0,0,0,0.15)',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.3,
        shadowRadius: 10,
        borderColor: colors.green,
        // elevation: 1,
    },
    placeholderStyle: {
        color: colors.darkBlue,
        // fontFamily: 'Poppins-Regular'
    },
    labelStyle: {
        fontSize: 18,
        textAlign: 'left',
        color: colors.darkBlue
    },
    component: {
        backgroundColor: 'transparent',
        paddingLeft: '58%',
        marginHorizontal:20,
        // borderWidth:1,
        // borderBottomWidth:2,
        borderColor: 'transparent',
        borderRadius: 0,
        // paddingVertical:12,
        // paddingHorizontal:10,

    },
    red: {
        color: colors.red
    },


})

export default styles
