import {Platform, StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'
import DropDownPicker from "react-native-dropdown-picker";

const styles = StyleSheet.create({
    container: {
        // paddingLeft: '5/8%',
        marginHorizontal:20,
        // borderWidth: 1,
        // width: '30%',
    },

    dropDownStyle: {
        // borderWidth:1,
        // borderTopWidth:1,
        // marginLeft: '100%',
        // marginLeft:10,
        borderWidth: 0,
        borderTopLeftRadius:10,
        borderTopRightRadius:2,
        borderBottomLeftRadius:2,
        borderBottomEndRadius:10,
        backgroundColor: colors.white,
        shadowColor: 'rgba(0,0,0,0.8)',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation:10,
        // borderBottomLeftRadius:10,
        borderColor: colors.green,
        ...(Platform.OS !== 'android' && {
            zIndex: 10
        })
        // elevation: 1,
    },
    placeholderStyle: {
        color: colors.darkBlue,
        fontSize: 14,
        // width: "100%"
        // fontFamily: 'Poppins-Regular'
    },
    labelStyle: {
        fontSize: 16,
        paddingVertical:3,
        textAlign: 'left',
        color: colors.darkBlue
    },
    component: {
        backgroundColor: 'transparent',
        // paddingLeft: '58%',
        // paddingHorizontal:20,
        // marginLeft:10,
        // marginVertical:10,
        borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        // width: '100%',
        // borderBottomWidth:2,
        borderColor: 'transparent',
        // borderRadius: 0,
        // paddingVertical:12,
        // paddingHorizontal:10,

    },
    red: {
        color: colors.red
    },


})

export default styles
