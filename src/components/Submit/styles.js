import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        // zIndex:2,
        marginVertical:7.5,
    },
    button:{
        // marginHorizontal:"9%",
        paddingVertical:21,
        justifyContent:'center',
        // borderRadius:100,
        borderRadius:5,
        // marginBottom:"5%",
        backgroundColor:'green',
        flexDirection:'row',

        // borderWidth: 1,

        // marginTop:'8%',
        // elevation:30
    },
    text:{
        color:colors.white,
        fontSize:16,
        fontFamily:'Rubik-Regular',
        width:'83%',
        textAlign:'center',

    }

})

export default styles
