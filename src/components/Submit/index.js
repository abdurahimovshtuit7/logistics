import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
import Arrow from 'assets/image/SVG/arrow'

const Submit = (props) => (
    <View style={[styles.container,props.style]}>
        <TouchableOpacity style={[styles.button,{backgroundColor: props.background,borderWidth:props.border?props.border:null,borderColor:props.border?colors.border:null,marginTop: props.padding}]}
                          onPress={props.touch}>
            <Text style={[styles.text,{color:props.color}]}>{props.text}</Text>
            <Arrow color={props.border||!props.icon?colors.green:colors.white}/>
        </TouchableOpacity>
    </View>
);
Submit.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    background:PropTypes.string,
    color:PropTypes.string,
    border:PropTypes.number,
    icon:PropTypes.bool,
    padding:PropTypes.number,
    // margin:PropTypes.string,

};
Submit.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
    background:'',
    color:'',
    border:null,
    icon:true,
    padding:0
    // margin:''
};
export default Submit;
