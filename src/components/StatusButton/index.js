import React, {useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
// import Person from 'assets/images/SVG/Person'
// import HandShake from 'assets/images/SVG/HandShake'

const Screen = (props) => {
    const [value, setValue] = useState('1')
    return (
        <View style={styles.view}>
            <View style={styles.circle}>
            </View>
            <TouchableOpacity style={styles.content} onPress={props.onSelected}>
                <Text style={styles.text1}>{props.text1}</Text>
                {
                    props.text2?(<Text style={styles.text2}>{props.text2}</Text>):( <Text style={styles.text2}>{props.placeholder}</Text>)
                }

            </TouchableOpacity>

        </View>)
};
Screen.propTypes = {
    style: PropTypes.object,
    // PROP: PropTypes.array,
    onSelected: PropTypes.func,
    text1:PropTypes.string,
    text2:PropTypes.string,
    placeholder:PropTypes.string
    // button:PropTypes.func
};
Screen.defaultProps = {
    style: {},
    // PROP: [],
    onSelected: function () {
    },
    text1:'',
    text2:'',
    placeholder:''

};
export default Screen;
