import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    view:{
        // marginTop:16,
        width:'100%',
        paddingHorizontal: 20,
        paddingVertical:10,
        // borderWidth:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    circle:{
        width:13,
        height:13,
        borderWidth: 4,
        borderRadius:20,
        borderColor:colors.whiteAlpha,
        // marginRight:10,
    },
    content:{
        backgroundColor:colors.white,
        width: '93%',
        flexDirection: 'row',
        borderRadius: 5,
        paddingVertical:20,
        paddingHorizontal: 15,
        // justifyContent:'center',
        //IOS
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    text1:{
        color:colors.grey,
        fontFamily:'Rubik-Regular',
        paddingRight:3,
    },
    text2:{
        color:colors.lightBlack,
        fontFamily:'Rubik-Regular',
        width:'60%',
        // borderWidth: 1,
    }




});

export default styles
