import { StyleSheet } from 'react-native';
import colors from '../../assets/styles/colors'
import DropDownPicker from "react-native-dropdown-picker";

const styles = StyleSheet.create({
    container:{
        // marginHorizontal:24,
        // backgroundColor:'rgba(255,255,255,0.61)',
        paddingVertical: 12,
        // zIndex:1,
        // borderWidth:1
    },
    title:{
        fontSize:14,
        fontFamily:'Rubik-Medium',
        color:colors.black,
        paddingBottom:11,
        // paddingBottom:8
    },
    dropDownStyle:{
        // borderWidth:1,
        borderTopWidth:1,
        borderWidth:0,
        backgroundColor:colors.white,
        shadowColor: 'rgba(0,0,0,0.15)',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 10,
        borderColor:colors.green,
        elevation: 1,
    },
    placeholderStyle:{
        color:colors.black,
        // fontFamily: 'Poppins-Regular'
    },
    labelStyle:{
        fontSize: 14,
        textAlign: 'left',
        color: '#000'
    },
    component:{
        backgroundColor:colors.white,
        marginBottom:20,
        borderWidth:1,
        // borderBottomWidth:2,
        borderColor: colors.green,
        borderRadius:0,
        paddingVertical:12,

    },
    red:{
        color: colors.red
    },



})

export default styles
