import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
import Arrow from 'assets/image/SVG/arrow'
import LinearGradient from "react-native-linear-gradient";

const Submit = (props) => (

    <View style={[styles.container,props.style,{paddingHorizontal:props.paddingHorizantal,marginTop: props.padding}]}>

        <TouchableOpacity
                          onPress={props.touch}>
            <LinearGradient colors={props.border?[colors.white,colors.white]:[colors.linearEnd,colors.linearBegin,]}
                            style={[styles.button,{backgroundColor: props.background,borderWidth:props.border?props.border:null,borderColor:props.border?colors.border:null}]}>
            {
                props.isActive?(<ActivityIndicator color={'#fff'} />):( <Text style={[styles.text,{color:props.color}]}>{props.text}</Text>)
            }
            </LinearGradient>
        </TouchableOpacity>

    </View>

);
Submit.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    background:PropTypes.string,
    color:PropTypes.string,
    border:PropTypes.number,
    icon:PropTypes.bool,
    padding:PropTypes.number,
    paddingHorizantal:PropTypes.number,
    isActive:PropTypes.bool
    // margin:PropTypes.string,

};
Submit.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
    background:'',
    color:'',
    border:null,
    icon:true,
    padding:0,
    paddingHorizantal:0,
    isActive:false
    // margin:''
};
export default Submit;
